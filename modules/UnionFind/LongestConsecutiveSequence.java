/*
Given a sequence of unsorted integers, find the length of the longest consecutive sequence
Input: nums = [0,3,7,2,5,8,4,6,0,1]
Output: 9
*/

import java.util.HashMap;

class LongestConsecutiveSequence {
    HashMap<Integer, Integer> hashmap = new HashMap<>();
    HashMap<Integer, Integer> size = new HashMap<>();

    int root(int value) {
        while (hashmap.get(value) != value) {
            hashmap.put(value, hashmap.get(hashmap.get(value)));
            value = hashmap.get(value);
        }

        return value;
    }

    void connect(int val1, int val2) {
        int val1Root = root(val1);
        int val2Root = root(val2);

        if (size.get(val1Root) > size.get(val2Root)) {
            hashmap.put(val2Root, val1Root);
            size.put(val1Root, size.get(val1Root) + size.get(val2Root));
        } else {
            hashmap.put(val1Root, val2Root);
            size.put(val2Root, size.get(val1Root) + size.get(val2Root));
        }
    }

    public int longestConsecutive(int[] nums) {

        for (int i = 0; i < nums.length; i++) {
            if (hashmap.containsKey(nums[i])) {
                continue;
            }
            hashmap.put(nums[i], nums[i]);
            size.put(nums[i], 1);
            if (hashmap.containsKey(nums[i] + 1)) {
                connect(nums[i], nums[i] + 1);
            }
            if (hashmap.containsKey(nums[i] - 1)) {
                connect(nums[i], nums[i] - 1);
            }

        }

        int max = 0;
        for (int key : size.keySet()) {
            if (max < size.get(key)) {
                max = size.get(key);
            }
        }

        return max;
    }

    public static void main(String[] args) {
        int[] nums = { 0, 3, 7, 2, 5, 8, 4, 6, 0, 1 };
        LongestConsecutiveSequence longestConsecutiveSequence = new LongestConsecutiveSequence();
        System.out.println(longestConsecutiveSequence.longestConsecutive(nums));
    }
}