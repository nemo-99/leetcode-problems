#include <bits/stdc++.h>
using namespace std;

/*
 * Design a class MedianFinder with the functionality to
 * add a number to the list
 * find median of the list of numbers
 */

class MedianFinder
{
public:
    /** initialize your data structure here. */
    priority_queue <int> maxq;
    priority_queue <int, vector <int>, greater<int> > minq;
    MedianFinder()
    {

    }

    void addNum(int num)
    {
        maxq.push(num);
        minq.push(maxq.top());
        maxq.pop();
        if(minq.size() > maxq.size())
        {
            maxq.push(minq.top());
            minq.pop();
        }
    }

    double findMedian()
    {
        if(minq.size() && maxq.size() == minq.size())
        {
            return (double(minq.top()) + maxq.top())/2;
        }
        else
        {
            return maxq.top();
        }
    }
};

int main()
{
}
