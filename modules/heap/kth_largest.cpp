#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

/*
 * Given an array, return the kth largest element
 * in the array
 */

int findKthLargest(vector<int>& nums, int k)
{
    make_heap(nums.begin(),nums.end());

    for(int i = 0;i < k-1;i++)
    {
        pop_heap(nums.begin(),nums.end());
        nums.pop_back();
    }

    return nums.front();
}

int main()
{
    vector <int> nums = {3,2,3,1,2,4,5,5,6};
    cout << findKthLargest(nums,4) << endl;
}

