#include <vector>
#include <iostream>
using namespace std;

/*
 * Implementation of a minheap with the following functionalities
 * getMini() - get minimum element
 * extractMini() - extract Minimum element
 * decreaseKey() - change value of key at a specific index
 * insert() - insert key into heap
 * delete() - delete value of key at specific index
 * display() - display the heap array
 */

class myheap
{
    vector <int> heap;
    int minvalue;

    void heapify(int i)
    {
        if(2*i + 1 < heap.size() && i >= 0)
        {
            int minidx;
            if(2*i + 2 < heap.size())
            {
                minidx = heap[2*i+1] < heap[2*i+2] ? 2*i+1 : 2*i+2;
            }
            else
            {
                minidx = 2*i+1;
            }
            if(heap[minidx] < heap[i])
            {
                swap(heap[minidx],heap[i]);
                heapify(minidx);
            }
        }
    }

public:
    myheap(vector<int> _arr)
    {
        heap = _arr;
        minvalue = 0;
        for(int i = _arr.size()/2 - 1;i >= 0;i--)
        {
            heapify(i);
        }
    }

    myheap(vector<int> _arr, int _minvalue)
    {
        heap = _arr;
        minvalue = _minvalue;
        minvalue = 0;
        for(int i = _arr.size()/2 - 1;i >= 0;i--)
        {
            heapify(i);
        }
    }

    void display()
    {
        cout << "[";
        for(int i : heap)
        {
            cout << i << ",";
        }
        cout << "]" << endl;
    }

    int getMini()
    {
        return heap[0];
    }

    int extractMini()
    {
        swap(heap.front(),heap.back());
        int ans = heap.back();
        heap.pop_back();
        heapify(0);
        return ans;
    }

    void decreaseKey(int i,int value)
    {
        heap[i] = value;
        while((i-1)/2 >= 0 && heap[i] < heap[(i-1)/2])
        {
            swap(heap[i],heap[(i-1)/2]);
            i = (i-1)/2;
        }
    }

    void insert(int value)
    {
        heap.push_back(value);
        int i = heap.size() - 1;
        while((i-1)/2 >= 0 && heap[i] < heap[(i-1)/2])
        {
            swap(heap[i],heap[(i-1)/2]);
            i = (i-1)/2;
        }
    }

    void delete_key(int i)
    {
        decreaseKey(i,minvalue-1);
        extractMini();
    }

};

int main()
{
    myheap * h = new myheap({5,9,8,6,3,1});
    cout << h->getMini() << endl;
    h->display();
    h->extractMini();
    h->display();
    h->delete_key(1);
    h->display();
    h->insert(1);
    h->display();
}
