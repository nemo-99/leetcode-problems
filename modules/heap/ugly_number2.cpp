#include <iostream>
#include <unordered_set>
#include <bits/stdc++.h>
using namespace std;

/*
 * A number whose only prime factors are 2,3 and 5
 * is called an ugly number.
 * Given an integer n, find the nth ugly number
 * Input: n = 10
 * Output: 12
 */

int nthUglyNumber(int n)
{
    if(n == 1)
    {
        return 1;
    }

    unordered_set <long> hs;
    priority_queue <long,vector<long>,greater <long>> q;

    q.push(1);
    int count = 1;
    while(count != n)
    {
        if(hs.count(q.top()*2) == 0)
        {
            q.push(q.top()*2);
            hs.insert(q.top()*2);
        }
        if(hs.count(q.top()*3) == 0)
        {
            q.push(q.top()*3);
            hs.insert(q.top()*3);
        }
        if(hs.count(q.top()*5) == 0)
        {
            q.push(q.top()*5);
            hs.insert(q.top()*5);
        }
        q.pop();
        count++;
    }

    return q.top();


}

int main()
{
    cout << nthUglyNumber(1407) << endl;
}
