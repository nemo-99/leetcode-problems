#include <unordered_map>
#include <vector>
#include <bits/stdc++.h>
using namespace std;

/*
 * Given a vector nums, and a sliding window of size k
 * find the maximum element for each sliding window
 * Input: nums = [1,3,-1,-3,5,3,6,7], k = 3
 * Output: [3,3,5,5,6,7]
 */

vector<int> maxSlidingWindow(vector<int>& nums, int k)
{
    priority_queue <int> q;
    unordered_map <int,int> hm;
    vector <int> ans;

    for(int i = 0; i < k;i++)
    {
        q.push(nums[i]);
        hm[nums[i]] = i;
    }

    ans.push_back(q.top());

    for(int i = 1;i <= nums.size() - k;i++)
    {
        q.push(nums[i+k-1]);
        hm[nums[i+k-1]] = i+k-1;
        while(hm[q.top()] < i)
        {
            q.pop();
        }
        ans.push_back(q.top());
    }

    return ans;
}

int main()
{
}
