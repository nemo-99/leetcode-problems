#include <unordered_set>
#include <bits/stdc++.h>
using namespace std;

/*
 * A Super Ugly number is one whose prime factors are
 * only those mentioned in the primes list.
 * Return the nth super ugly number
 * Input: n = 12, primes = [2,7,13,19]
 * Output: 32
 */

int nthSuperUglyNumber(int n, vector<int>& primes)
{
    if(n == 1)
    {
        return 1;
    }

    unordered_set <long> hs;
    priority_queue <long,vector<long>,greater <long>> q;

    q.push(1);
    int count = 1;
    while(count != n)
    {
        for(int prime : primes)
        {
            if(hs.count(q.top()*prime) == 0)
            {
                if(q.top()*prime > INT_MAX)
                {
                    break;
                }
                q.push(q.top()*prime);
                hs.insert(q.top()*prime);
            }
        }

        q.pop();
        count++;
    }

    return q.top();
}

int main()
{
}

