#include <bits/stdc++.h>
#include <unordered_map>
using namespace std;

/*
 * For problem description go to link
 * https://leetcode.com/problems/the-skyline-problem/
 */

vector<vector<int>> getSkyline(vector<vector<int>>& buildings)
{
    int bld = 0;
    vector <vector<int>> ans;
    unordered_map <int,int> hm;
    priority_queue <int> q;
    int max = 0;

    for(int i = buildings[0][0];;)
    {
        while(bld < buildings.size() && i == buildings[bld][0])
        {
            q.push(buildings[bld][2]);
            hm[buildings[bld][2]] = bld;
            bld++;
        }

        if(max < q.top())
        {
            ans.push_back({i,q.top()});
            max = q.top();
        }

        while(!q.empty() && buildings[hm[q.top()]][1] <= i)
        {
            if(q.size() == 1)
            {
                ans.push_back({i,0});
            }
            q.pop();
        }

        if(q.empty())
        {
            max = 0;
        }
        else if(max != q.top())
        {
            ans.push_back({i,q.top()});
            max = q.top();
        }

        if(q.empty() && bld == buildings.size())
        {
            break;
        }
        else if(!q.empty() && bld == buildings.size())
        {
            i = buildings[hm[q.top()]][1];
        }
        else if(!q.empty())
        {
            i = buildings[bld][0] < buildings[hm[q.top()]][1] ? buildings[bld][0] : buildings[hm[q.top()]][1];
        }
        else
        {
            i = buildings[bld][0];
        }
    }

    return ans;
}

int main()
{
}
