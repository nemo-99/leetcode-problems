#include <unordered_map>
#include <algorithm>
#include <vector>
using namespace std;

/*
 * Design a simplified version of twitter
 */

class Twitter
{
public:
    /** Initialize your data structure here. */
    unordered_map <int,vector <int> > user_feed;
    unordered_map <int,int> tweet_user;
    unordered_map <int,vector <int>> user_tweets;
    unordered_map <int, vector <int>> user_followers;
    unordered_map <int,int> count_tweet;
    int count;
    Twitter()
    {
        count = 0;
    }

    /** Compose a new tweet. */
    void postTweet(int userId, int tweetId)
    {
        count++;
        user_tweets[userId].push_back(count);
        user_feed[userId].push_back(count);
        push_heap(user_feed[userId].begin(),user_feed[userId].end());
        for(int follower : user_followers[userId])
        {
            user_feed[follower].push_back(count);
            push_heap(user_feed[follower].begin(),user_feed[follower].end());
        }

        tweet_user[count] = userId;
        count_tweet[count] = tweetId;
    }

    /** Retrieve the 10 most recent tweet ids in the user's news feed. Each item in the news feed must be posted by users who the user followed or by the user herself. Tweets must be ordered from most recent to least recent. */
    vector<int> getNewsFeed(int userId)
    {
        vector <int> tmp = user_feed[userId];
        vector <int> feed;
        int i = 10;
        while(i > 0 && !tmp.empty())
        {
            feed.push_back(count_tweet[tmp[0]]);
            pop_heap(tmp.begin(),tmp.end());
            tmp.pop_back();
            i--;
        }
        return feed;
    }

    /** Follower follows a followee. If the operation is invalid, it should be a no-op. */
    void follow(int followerId, int followeeId)
    {
        if(followerId == followeeId)
        {
            return;
        }
        for(int follower : user_followers[followeeId])
        {
            if(followerId == follower)
            {
                return;
            }
        }

        user_followers[followeeId].push_back(followerId);
        for(int tweet : user_tweets[followeeId])
        {
            user_feed[followerId].push_back(tweet);
            push_heap(user_feed[followerId].begin(),user_feed[followerId].end());
        }
    }

    /** Follower unfollows a followee. If the operation is invalid, it should be a no-op. */
    void unfollow(int followerId, int followeeId)
    {
        if(followerId == followeeId)
        {
            return;
        }
        int i;
        for(i = 0;i < user_followers[followeeId].size();i++)
        {
            if(followerId == user_followers[followeeId][i])
            {
                break;
            }
        }
        if(i != user_followers[followeeId].size())
        {
            user_followers[followeeId].erase(user_followers[followeeId].begin() + i);
            for(int i = user_feed[followerId].size() - 1; i >= 0;i--)
            {
                if(tweet_user[user_feed[followerId][i]] == followeeId)
                {
                    user_feed[followerId].erase(user_feed[followerId].begin() + i);
                }
            }
            make_heap(user_feed[followerId].begin(),user_feed[followerId].end());
        }

    }
};

int main()
{
}
