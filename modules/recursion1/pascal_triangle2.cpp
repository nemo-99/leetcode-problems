#include <vector>
#include <unordered_map>
using namespace std;

/*
 * Given rowIndex, return pascal triangle of the
 * corresponding row. Index starts from 0
 */

int calc(int i,int j,unordered_map <string,int> & hm )
{
    if(j == 0 || i == j)
    {
        return 1;
    }
    else
    {
        string ij = to_string(i) + "," + to_string(j);
        if(hm.count(ij) != 0)
        {
            return hm[ij];
        }
        else
        {
            hm[ij] = calc(i-1,j-1,hm) + calc(i-1,j,hm);
            return hm[ij];
        }
    }
}

vector<int> getRow(int rowIndex)
{
    vector <int> ans;
    unordered_map <string,int> hm;
    for(int i = 0;i <= rowIndex;i++)
    {
        if(i == 0 || i == rowIndex)
        {
            ans.push_back(1);
        }
        else
        {
        ans.push_back(calc(rowIndex-1,i-1,hm) + calc(rowIndex-1,i,hm));
        }
    }
    return ans;
}

int main()
{
}
