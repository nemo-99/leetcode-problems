#include <unordered_map>
using namespace std;

/*
 * Given a step number(n), return the total distinct
 * ways to reach it given that there are two ways to
 * climb steps, one at a time and two at a time
 */

int recursive(int n, unordered_map <int,int> & hm)
{
    if(n == 2)
    {
        return 2;
    }
    if(n == 1)
    {
        return 1;
    }

    int sum = 0;
    for(int i = 1;i <= 2;i++)
    {
        int fpart;
        if(hm.count(n-i) != 0)
        {
            fpart = hm[n-i];
        }
        else
        {
            fpart = recursive(n-i,hm);
            hm[n-i] = fpart;
        }

        sum += fpart;
    }

    return sum;
}

int climbStairs(int n)
{
    unordered_map <int,int> hm;
    return recursive(n,hm);
}

int main()
{
}
