#include <iostream>
using namespace std;

/*
 * Implement pow function -
 * Given two numbers x and n,
 * return x**n
 */

double recursion(double x,unsigned int n)
{
    if(n == 0)
    {
        return 1;
    }

    double ans = recursion(x,n/2);
    ans = ans * ans;
    if(n % 2 != 0)
    {
        ans = ans * x;
    }

    return ans;
}

double myPow(double x, int n)
{
    if(x == 1)
    {
        return 1;
    }
    return n < 0 ? 1/recursion(x,-(unsigned int)n) : recursion(x,n);
}

int main()
{
    cout << myPow(2,-2) << endl;
}
