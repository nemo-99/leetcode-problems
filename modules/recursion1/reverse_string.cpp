#include <vector>
using namespace std;

/*
 * Reverse a list of characters in place recursively
 */

void revstr(vector <char>& s, int i,int j)
{
    if(i >= j)
    {
        return;
    }
    swap(s[i],s[j]);
    revstr(s,i+1,j-1);
}

void reverseString(vector<char>& s)
{
    revstr(s,0,s.size()-1);
}

int main()
{
}
