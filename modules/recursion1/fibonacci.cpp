#include <unordered_map>
using namespace std;

/*
 * Return the nth fibonacci number
 */

int recursive(int n, unordered_map <int,int> & hm)
{
    if(n == 0)
    {
        return 0;
    }
    if(n == 1 || n == 2)
    {
        return 1;
    }

    int sum = 0;
    for(int i = 1;i <= 2;i++)
    {
        int fpart;
        if(hm.count(n-i) != 0)
        {
            fpart = hm[n-i];
        }
        else
        {
            fpart = recursive(n-i,hm);
            hm[n-i] = fpart;
        }

        sum += fpart;
    }

    return sum;
}

int fib(int n)
{
    unordered_map <int,int> hm;
    return recursive(n,hm);
}

int main()
{
}
