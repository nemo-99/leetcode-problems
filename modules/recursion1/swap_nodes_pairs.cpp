#include <iostream>

/*
 * Given a linked list, swap nodes of the linked list in
 * pairs
 * Input: head = [1,2,3,4]
 * Output: [2,1,4,3]
 */

struct ListNode {
      int val;
      ListNode *next;
      ListNode() : val(0), next(nullptr) {}
      ListNode(int x) : val(x), next(nullptr) {}
      ListNode(int x, ListNode *next) : val(x), next(next) {}
  };

void swapNodes(ListNode * & buff)
{
    ListNode * tmp = buff;
    buff = buff->next;
    tmp->next = buff->next;
    buff->next = tmp;
    if(tmp->next && tmp->next->next)
    {
        swapNodes(tmp->next);
    }
}

ListNode* swapPairs(ListNode* head)
{
    if(head == NULL)
    {
        return NULL;
    }
    else if(head->next == NULL)
    {
        return head;
    }

    swapNodes(head);
    return head;
}

int main()
{
}
