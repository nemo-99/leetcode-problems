#include <iostream>

/*
 * Search for a given val in a given Binary Search Tree
 * If val is found, return the node, else return NULL
 */

struct TreeNode {
      int val;
      TreeNode *left;
      TreeNode *right;
      TreeNode() : val(0), left(nullptr), right(nullptr) {}
      TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
      TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
  };

TreeNode* searchBST(TreeNode* root, int val)
{
    if(root == NULL)
    {
        return NULL;
    }
    else if(root->val == val)
    {
        return root;
    }
    else if(val > root->val)
    {
        return searchBST(root->right,val);
    }
    else
    {
        return searchBST(root->left,val);
    }
}

int main()
{
}
