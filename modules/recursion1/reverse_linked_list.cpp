#include <iostream>

/*
 * Recursively reverse a linked list
 */

struct ListNode {
      int val;
      ListNode *next;
      ListNode() : val(0), next(nullptr) {}
      ListNode(int x) : val(x), next(nullptr) {}
      ListNode(int x, ListNode *next) : val(x), next(next) {}
  };

void reverse(ListNode * & head, ListNode * buff)
{
    ListNode * tmp = new ListNode(buff->next->val,head);
    buff->next = buff->next->next;
    head = tmp;
    if(buff->next)
    {
        reverse(head,buff);
    }
}

ListNode* reverseList(ListNode* head)
{
    if(head == NULL)
    {
        return NULL;
    }
    else if(head->next == NULL)
    {
        return head;
    }

    reverse(head,head);
    return head;
}

int main()
{
}
