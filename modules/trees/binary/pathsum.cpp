#include <iostream>

/*
 * Determine if root to leaf sum of a binary
 * tree equals a given value
 */

struct TreeNode {
      int val;
      TreeNode *left;
      TreeNode *right;
      TreeNode() : val(0), left(nullptr), right(nullptr) {}
      TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
      TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
  };

bool recursive(TreeNode* root,int sum, int target)
{
    if(root->left == NULL && root->right == NULL)
    {
        return root->val + sum == target;
    }
    bool val1,val2;
    if(root->left)
    {
        val1 = recursive(root->left,root->val + sum,target);
    }
    if(root->right)
    {
        val2 = recursive(root->right,root->val+sum,target);
    }
    return  val1 || val2; 
}

bool hasPathSum(TreeNode* root, int sum) {
    if(root == NULL)
    {
        return false;
    }

    return recursive(root,0,sum);
        
    }

int main()
{
}
