#include <iostream>
#include <vector>
using namespace std;

/*
 * Post order traversal of binary tree
 * done both iteratively and recursively
 */

struct TreeNode {
      int val;
      TreeNode *left;
      TreeNode *right;
      TreeNode() : val(0), left(nullptr), right(nullptr) {}
      TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
      TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
  };

void recursive(TreeNode * root,vector <int> &ans)
{
    if(root->left)
    {
        recursive(root->left,ans);
    }
    if(root->right)
    {
        recursive(root->right,ans);
    }
    ans.push_back(root->val);
}


vector<int> postorderTraversal(TreeNode* root) {
    if(root == NULL)
    {
        return {};
    }
    vector <int> ans;
    recursive(root,ans);
    return ans;
        
    }


vector <int> iterative(TreeNode* root)
{
    if(root == NULL)
    {
        return {};
    }
    vector <TreeNode *> stk;
    stk.push_back(root);
    vector <int> ans;
    vector <int> visited;
    visited.push_back(0);

    while(!stk.empty())
    {
        if(stk.back()->left && visited.back() == 0)
        {
            visited.back()++;
            stk.push_back(stk.back()->left);
            visited.push_back(0);
            continue;
        }
        else if(stk.back()->left == NULL)
        {
            visited.back()++;
        }

        if(stk.back()->right && visited.back() == 1)
        {
            visited.back()++;
            stk.push_back(stk.back()->right);
            visited.push_back(0);
            continue;
        }
        else if(stk.back()->right == NULL)
        {
            visited.back()++;
        }

        if(visited.back() >= 2)
        {
            ans.push_back(stk.back()->val);
            stk.pop_back();
            visited.pop_back();
        }
    }

    return ans;


}

int main()
{
}
