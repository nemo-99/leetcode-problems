#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

/*
 * Construct binary tree from inorder and 
 * preorder traversal
 */

struct TreeNode {
      int val;
      TreeNode *left;
      TreeNode *right;
      TreeNode() : val(0), left(nullptr), right(nullptr) {}
      TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
      TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
  };

void recursive(TreeNode * & root,int b,int e,vector<int> i,vector <int> & pre)
{
    if(pre.empty())
    {
        return;
    }
    
    if(b > e)
    {
        return;
    }
    if(b == e)
    {
        root = new TreeNode(i[b]);
        pre.erase(pre.begin());
        return;
    }

    vector <int>:: iterator it = find(i.begin() + b,i.begin() + e,pre.front());
    int idx = distance(i.begin(),it);
    pre.erase(pre.begin());
    root = new TreeNode(i[idx]);
    
    recursive(root->left,b,idx-1,i,pre);
    recursive(root->right,idx+1,e,i,pre);
}

TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder) {
    if(inorder.empty() || preorder.empty())
    {
        return NULL;
    }
    TreeNode * root;
   
    recursive(root,0,inorder.size()-1,inorder,preorder); 

    return root;
}

int main()
{
}
