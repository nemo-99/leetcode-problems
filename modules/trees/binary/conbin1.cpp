#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

/*
 * Given postorder and inorder traversal 
 * of a binary tree, construct the binary tree
 */

struct TreeNode {
      int val;
      TreeNode *left;
      TreeNode *right;
      TreeNode() : val(0), left(nullptr), right(nullptr) {}
      TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
      TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
  };

void recursive(TreeNode * & root,int b,int e,vector<int> i,vector <int> & po)
{
    if(po.empty())
    {
        return;
    }
    
    if(b > e)
    {
        return;
    }

    if(b == e)
    {
        root = new TreeNode(i[b]);
        po.pop_back();
        return;
    }

    vector <int>:: iterator it = find(i.begin() + b,i.begin() + e,po.back());
    int idx = distance(i.begin(),it);
    po.pop_back();
    root = new TreeNode(i[idx]);

    recursive(root->right,idx+1,e,i,po);
    recursive(root->left,b,idx-1,i,po);

}

TreeNode* buildTree(vector<int>& inorder, vector<int>& postorder) {
    if(inorder.empty() || postorder.empty())
    {
        return NULL;
    }

    TreeNode * root;
    recursive(root,0,inorder.size()-1,inorder,postorder); 

    return root;
        
    }

int main()
{
}
