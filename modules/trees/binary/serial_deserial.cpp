#include <iostream>
#include <string>
using namespace std;

/*
 * Convert a binary tree into a string representation
 * and then construct an identical tree from the string
 * representation
 */

struct TreeNode {
      int val;
      TreeNode *left;
      TreeNode *right;
      TreeNode(int x) : val(x), left(NULL), right(NULL) {}
  };

void recursive_serial(TreeNode * root,string & serial)
{
    if(root == NULL)
    {
        serial += "null,";
        return;
    }
    else
    {
        serial += to_string(root->val) + ",";
        recursive_serial(root->left,serial);
        recursive_serial(root->right,serial);
    }
}

string serialize(TreeNode* root) 
{
    string serial = "[";
    recursive_serial(root,serial);
    serial[serial.size()-1] = ']';
    return serial;
}

void recursive_deserial(TreeNode * & root, string str, int &i)
{
    if(root == NULL)
    {
        return;
    }
        
    if(str[i] != 'n')
    {
        string tmp = "";
        while(str[i] != ',')
        {
            tmp += str[i];
            i++;
        }
        root->left = new TreeNode(stoi(tmp));
        i++;
    }
    else
    {
        root->left = NULL;
        i+=5;
    }

    recursive_deserial(root->left,str,i);
        
    if(str[i] != 'n')
    {
        string tmp = "";
        while(str[i] != ',')
        {
            tmp += str[i];
            i++;
        }
        root->right = new TreeNode(stoi(tmp));
        i++;
    }
    else
    {
        root->right = NULL;
        i+=5;
    }
    recursive_deserial(root->right,str,i);
}

TreeNode* deserialize(string data) 
{
    if(data[1] == 'n')    
    {
        return NULL;
    }
    TreeNode * root;
    int i = 1;
    string tmp = "";
    while(data[i] != ',')
    {
        tmp += data[i];
        i++;
    }
    root = new TreeNode(stoi(tmp));
    recursive_deserial(root,data,++i);
    
    return root;
}

int main()
{
}
