#include <iostream>

/*
 * Return maximum depth of a binary tree
 * Implemented using both top-down and 
 * bottom up methods
 */

struct TreeNode {
      int val;
      TreeNode *left;
      TreeNode *right;
      TreeNode() : val(0), left(nullptr), right(nullptr) {}
      TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
      TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
  };


// top-down method
int recursive(TreeNode * root,int depth)
{
    if(root == NULL)
    {
        return depth;
    }

    int left_depth = recursive(root->left,depth+1);
    int right_depth = recursive(root->right,depth+1);
    return left_depth > right_depth ? left_depth : right_depth;

}

int maxDepth(TreeNode* root) {
    if(root == NULL)
    {
        return 0;
    }

    int ans = recursive(root,0);
    return ans;
}

//bottom-up method
int maxDepth_2(TreeNode* root)
{
    if(root == NULL)
    {
        return 0;
    }

    int left_depth = maxDepth(root->left) + 1;
    int right_depth = maxDepth(root->right) + 1;
        
    return left_depth > right_depth ? left_depth : right_depth;
    
}

int main()
{
}
