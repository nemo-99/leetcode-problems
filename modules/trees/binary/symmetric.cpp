#include <iostream>
#include <queue>
using namespace std;

/*
 * Check if binary tree is a mirror of itself
 */

struct TreeNode {
      int val;
      TreeNode *left;
      TreeNode *right;
      TreeNode() : val(0), left(nullptr), right(nullptr) {}
      TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
      TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
  };

// iterative solution
bool isSymmetric(TreeNode* root) 
{
    if(root == NULL)
    {
        return true;
    }
    if(root->left == NULL && root->right == NULL)
    {
        return true;
    }
    else if(root->left == NULL || root->right == NULL)
    {
        return false;
    }
    queue <TreeNode *> lq;
    queue <TreeNode *> rq;
    lq.push(root->left);
    rq.push(root->right);

    while(!lq.empty() && !rq.empty())
    {
        if(lq.front()->val != rq.front()->val)
        {
            return false;
        }

        if(lq.front()->left && rq.front()->right)
        {
            lq.push(lq.front()->left);
            rq.push(rq.front()->right);
        }
        else if(lq.front()->left || rq.front()->right)
        {
            return false;
        }

        if(lq.front()->right && rq.front()->left)
        {
            lq.push(lq.front()->right);
            rq.push(rq.front()->left);
        }
        else if(lq.front()->right || rq.front()->left)
        {
            return false;
        }

        lq.pop();
        rq.pop();
    }

    if(lq.empty() && rq.empty())
    {
        return true;
    }

    return false;

}

//recursive solution
bool recursive(TreeNode * ln, TreeNode * rn)
{
    if(ln == NULL && rn == NULL)
    {
        return true;
    }
    else if(ln->val == rn->val)
    {
        return recursive(ln->left,rn->right) && recursive(ln->right,rn->left);
    }
    else
    {
        return false;
    }
}

bool isSymmetric_rec(TreeNode * root)
{
    if(root == NULL)
    {
        return true;
    }
    return recursive(root->left,root->right);
}

int main()
{
}
