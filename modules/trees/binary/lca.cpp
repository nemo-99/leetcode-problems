#include <iostream>
#include <queue>
using namespace std;

/*
 * Find the lowest common ancestor of two nodes with 
 * given values, assuming both the values exist and
 * all the node values are unique
 */

struct TreeNode {
      int val;
      TreeNode *left;
      TreeNode *right;
      TreeNode(int x) : val(x), left(NULL), right(NULL) {}
  };

int recursive(TreeNode * node,queue <TreeNode *> & path, int target)
{
    if(node == NULL)
    {
        return 0;
    }
    if(node->val == target)
    {
        path.push(node);
        return 1;
    }

    if(recursive(node->left,path,target))
    {
        path.push(node);
        return 1;
    }
    else if(recursive(node->right,path,target))
    {
        path.push(node);
        return 1;
    }

    return 0;
    return 1;
}

TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) 
{
    queue <TreeNode *> p_path;
    queue <TreeNode *> q_path;
    recursive(root,p_path,p->val);
    recursive(root,q_path,q->val);

    queue <TreeNode *> bigq = p_path.size() > q_path.size() ? p_path : q_path;
    queue <TreeNode *> smallq = p_path.size() <= q_path.size() ? p_path : q_path;

    int size = bigq.size();
    while(size > smallq.size())
    {
        bigq.pop();
        size--;
    }

    while(bigq.size() > 0)
    {
        if(bigq.front()->val == smallq.front()->val)
        {
            return bigq.front();
        }
        
        bigq.pop();
        smallq.pop();
    }
    return bigq.front();
}

int main()
{
}
