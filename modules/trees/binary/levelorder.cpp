#include <iostream>
#include <queue>
#include <math.h>
using namespace std;

/*
 * Binary tree level order traversal
 */

struct TreeNode {
      int val;
      TreeNode *left;
      TreeNode *right;
      TreeNode() : val(0), left(nullptr), right(nullptr) {}
      TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
      TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
  };

vector<vector<int>> levelOrder(TreeNode* root) {
    if(root == NULL)
    {
        return {};
    }
    vector <vector <int>> ans;
    vector <int> layer;
    int newcount = 0;
    int count = 1;
    queue <TreeNode *> q;
    q.push(root);

    while(!q.empty())
    {
        layer.push_back(q.front()->val);
        if(q.front()->left)
        {
            q.push(q.front()->left);
            newcount++;
        }
        if(q.front()->right)
        {
            q.push(q.front()->right);
            newcount++;
        }
        q.pop();
        count--;
        if(count == 0)
        {
            ans.push_back(layer);
            layer.clear();
            count = newcount;
            newcount = 0;
        }

    }
    
    return ans;
        
    }

int main()
{
}
