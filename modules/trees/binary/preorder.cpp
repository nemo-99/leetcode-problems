#include <iostream>
#include <vector>
using namespace std;

/*
 * Preorder traversal of binary tree
 * using both recursive and iterative 
 * methods
 */

struct TreeNode {
      int val;
      TreeNode *left;
      TreeNode *right;
      TreeNode() : val(0), left(nullptr), right(nullptr) {}
      TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
      TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
  };

void recursive(TreeNode * root, vector <int> & ans)
{
    ans.push_back(root->val);
    if(root->left)
    {
        recursive(root->left,ans);
    }
    if(root->right)
    {
        recursive(root->right,ans);
    }
}

vector<int> preorderTraversal(TreeNode* root) {
       if(root == NULL)
       {
           return {};
       }

       vector <int> ans;
       recursive(root,ans);
       return ans;
           
    }

vector <int> iterative_traversal(TreeNode * root)
{
    if(root == NULL)
    {
        return {};
    }
    vector <TreeNode *> stk;
    vector <int> visited;
    vector <int> ans;
    stk.push_back(root);
    visited.push_back(0);
    ans.push_back(root->val); 
    while(stk.size() != 0)
    {
        if(stk.back()->left && visited.back() == 0)
        {
            visited.back()++;
            ans.push_back(stk.back()->left->val);
            if(stk.back()->left->left || stk.back()->left->right)
            {
                stk.push_back(stk.back()->left);
                visited.push_back(0);
                continue;
            }
        }
        else if(stk.back()->left == NULL)
        {
            visited.back()++;
        }

        if(stk.back()->right && visited.back() == 1)
        {
            visited.back()++;
            ans.push_back(stk.back()->right->val);
            if(stk.back()->right->left || stk.back()->right->right)
            {
                stk.push_back(stk.back()->right);
                visited.push_back(0);
                continue;
            }
        }
        else if(stk.back()->right == NULL)
        {
            visited.back()++;
        }

        while(visited.back() == 2)
        {
            stk.pop_back();
            visited.pop_back();
            if(visited.size() == 0)
            {
                return ans;
            }
        }
        
    }
    return ans;
}

int main()
{
}






