#include <iostream>

/*
 * Given a binary tree, populate each next pointer 
 * to the node to its right
 * Binary Tree may not be perfect
 */

class Node {
public:
    int val;
    Node* left;
    Node* right;
    Node* next;

    Node() : val(0), left(NULL), right(NULL), next(NULL) {}

    Node(int _val) : val(_val), left(NULL), right(NULL), next(NULL) {}

    Node(int _val, Node* _left, Node* _right, Node* _next)
        : val(_val), left(_left), right(_right), next(_next) {}
};

void recursive(Node *& buff)
{
    if(buff == NULL)
    {
        return;
    }

    if(buff->left)
    {
        if(buff->right)
        {
            buff->left->next = buff->right;
        }
        else
        {
            Node * tmp = buff->next;
            while(tmp)
            {
                if(tmp->left)
                {
                    buff->left->next = tmp->left;
                    break;
                }
                if(tmp->right)
                {
                    buff->left->next = tmp->right;
                    break;
                }
                tmp = tmp->next;
            }
        }
        
    }
    
    if(buff->next && buff->right)
    {
        Node * tmp = buff->next;
        while(tmp)
        {
            if(tmp->left)
            {
                buff->right->next = tmp->left;
                break;
            }
            if(tmp->right)
            {
                buff->right->next = tmp->right;
                break;
            }
            tmp = tmp->next;
        }
        
    }
        
    recursive(buff->right);
    recursive(buff->left);
    
}

Node* connect(Node* root)
{
    recursive(root);
    return root;
}

int main()
{
}
