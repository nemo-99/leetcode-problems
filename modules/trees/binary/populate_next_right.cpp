#include <iostream>

/*
 * Populate each next pointer of a binary tree node
 * to the node to its right
 */

class Node {
public:
    int val;
    Node* left;
    Node* right;
    Node* next;

    Node() : val(0), left(NULL), right(NULL), next(NULL) {}

    Node(int _val) : val(_val), left(NULL), right(NULL), next(NULL) {}

    Node(int _val, Node* _left, Node* _right, Node* _next)
        : val(_val), left(_left), right(_right), next(_next) {}
};

void recursive(Node *& buff)
{
    if(buff->left == NULL)
    {
        return;
    }

    buff->left->next = buff->right;
    if(buff->next)
    {
        buff->right->next = buff->next->left;
    }

    recursive(buff->left);
    recursive(buff->right);
}

Node* connect(Node* root) {
    if(root == NULL)
    {
        return root;
    }
    recursive(root);
    return root;
}

int main()
{
}
