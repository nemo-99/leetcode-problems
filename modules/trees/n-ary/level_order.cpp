#include <iostream>
#include <vector>
using namespace std;

/*
 * Iterative approach to
 * Level order traversal of n-ary tree
 */

class Node {
public:
    int val;
    vector<Node*> children;

    Node() {}

    Node(int _val) {
        val = _val;
    }

    Node(int _val, vector<Node*> _children) {
        val = _val;
        children = _children;
    }
};

vector<vector<int>> levelOrder(Node* root) {
    if(root == NULL)
    {
        return {};
    }
    vector <vector <int>>ans = {{root->val}};
    vector <int> layer;
    vector <Node *> q;
    q.push_back(root);
    int count = root->children.size();
    int newcount = 0;

    while(count != 0)
    {
        for(int i = 0;i < q.front()->children.size();i++)
        {
            layer.push_back(q.front()->children[i]->val);
            newcount += q.front()->children[i]->children.size();
            q.push_back(q.front()->children[i]);
            count--;
        }
        q.erase(q.begin());

        if(count == 0)
        {
            ans.push_back(layer);
            layer.clear();
            count = newcount;
            newcount = 0;
        }
    }

    return ans;

}

int main()
{
}
