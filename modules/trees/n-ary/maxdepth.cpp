#include <iostream>
#include <vector>
using namespace std;

/*
 * Given an n-ary tree,
 * find its maximum depth
 */

class Node {
public:
    int val;
    vector<Node*> children;

    Node() {}

    Node(int _val) {
        val = _val;
    }

    Node(int _val, vector<Node*> _children) {
        val = _val;
        children = _children;
    }
};

int find_depth(Node *root,int depth)
{
    int ans = 0;
    if(root->children.size() == 0)
    {
       return depth+1;
    }
    for(int i = 0;i < root->children.size();i++)
    {
        int tmp_depth = find_depth(root->children[i],depth+1);
        if(tmp_depth > ans)
        {
            ans = tmp_depth;
        }
    }
    return ans;
}


int maxDepth(Node* root) {
    if(root == NULL)
    {
        return 0;
    }

    int ans = find_depth(root,0);
    return ans;
}
