#include <vector>
using namespace std;

/* n-ary tree preorder traversal
 * recursive + iterative
 */

class Node {
public:
    int val;
    vector<Node*> children;

    Node() {}

    Node(int _val) {
        val = _val;
    }

    Node(int _val, vector<Node*> _children) {
        val = _val;
        children = _children;
    }
};

void recursive_traverse(Node * root, vector <int> & ans)
{
    ans.push_back(root->val);
    for(int i = 0;i < root->children.size();i++)
    {
        recursive_traverse(root->children[i],ans);
    }
}

vector<int> preorder(Node* root) {
    vector <int> ans;
    if(root == NULL)
    {
        return ans;
    }
    recursive_traverse(root,ans);
    return ans;
}

vector <int> preorder_iterative(Node * root)
{
    vector < Node * > q;
    vector < int > q_idx;
    vector <int> ans;
    if(root == NULL)
    {
        return ans;
    }
    q.push_back(root);
    q_idx.push_back(0);
    ans.push_back(root->val);

    for(int i = 0;i < q.back()->children.size();i++)
    {
        ans.push_back(q.back()->children[i]->val);
        if(q.back()->children[i]->children.size() != 0)
        {
            q_idx.back() = i;
            q.push_back(q.back()->children[i]);
            i = -1;
        }
        while(i == q.back()->children.size()-1)
        {
            q.pop_back();
            q_idx.pop_back();
            if(q_idx.empty())
            {
                return ans;
            }
            i = q_idx.back();
        }
    }

    return ans;

}

int main()
{
}
