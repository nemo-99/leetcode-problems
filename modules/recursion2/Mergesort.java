package modules.recursion2;

/*
    Sort a given array using mergesort
*/

public class Mergesort {
    public int[] sortArray(int[] nums) {

        int newNums[] = recursiveDivide(0, nums.length - 1, nums);

        return newNums;
    }

    public int[] recursiveDivide(int start, int end, int[] nums) {

        if (start == end) {
            int[] singleArray = new int[1];
            singleArray[0] = nums[start];
            return singleArray;
        }
        int[] leftArray = recursiveDivide(start, start + (end - start) / 2, nums);
        int[] rightArray = recursiveDivide(start + (end - start) / 2 + 1, end, nums);
        return merge(leftArray, rightArray);
    }

    public int[] merge(int[] leftArray, int[] rightArray) {
        int[] newArray = new int[leftArray.length + rightArray.length];
        int newNumsCounter = 0, leftArrayCounter = 0, rightArrayCounter = 0;

        while (leftArrayCounter < leftArray.length && rightArrayCounter < rightArray.length) {
            if (leftArray[leftArrayCounter] <= rightArray[rightArrayCounter]) {
                newArray[newNumsCounter++] = leftArray[leftArrayCounter++];
            } else {
                newArray[newNumsCounter++] = rightArray[rightArrayCounter++];
            }
        }

        while (leftArrayCounter < leftArray.length) {
            newArray[newNumsCounter++] = leftArray[leftArrayCounter++];
        }

        while (rightArrayCounter < rightArray.length) {
            newArray[newNumsCounter++] = rightArray[rightArrayCounter++];
        }

        return newArray;
    }

    public static void main(String[] args) {
        Mergesort mergesort = new Mergesort();
        int nums[] = { 5, 1, 1, 2, 0, 0 };
        int newNums[] = mergesort.sortArray(nums);

        for (int i : newNums) {
            System.out.print(i + " ");
        }

        System.out.println();
    }
}
