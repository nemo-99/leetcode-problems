'''
    Return name,population,area of countries with area greator than 3 million and
    population greator than 25 million

 '''
select name,
    population,
    area
from world
where area > 3000000
    or population > 25000000;