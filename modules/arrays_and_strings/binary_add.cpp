#include <iostream>
#include <vector>
using namespace std;

/*
 * Add two binary numbers in string form
 * a = "1010" b = "11"
 * ans = "1101"
 *
 */

void add(char & c1,char & c2,char& car,string & answer)
{
    string check = "";
    check += c1;
    check += c2;
    check += car;
    //cout << check << endl;
    int num1 = 0;
    for(int i = 0;i < check.length();i++)
    {
        if(check[i] == '1')
        {
            num1++;
        }
    }
    //cout << num1 << endl;
    if(num1 == 0)
    {
        answer = '0' + answer;
        car = '0';
    }
    else if(num1 == 1)
    {
        answer = '1' + answer;
        car = '0';
    }
    else if(num1 == 2)
    {
        answer = '0' + answer;
        car = '1';
    }
    else if(num1 == 3)
    {
        answer = '1' + answer;
        car = '1';
    }
    //cout << answer << car << endl;
    //cout << answer << endl;
    //cout << car << endl;
        
}
int main()
{
    string a = "1010";
    string b = "1011";
    string ans = "";
    int i = a.length() -1;
    int j = b.length() - 1;
    char carry = '0';
    int count = 0;
    do{
        if(i < 0 && j < 0)
        {
           ans = carry + ans; 
           carry = '0';
        }
        else if(i >= 0 && j >= 0)
        {
           add(a[i],b[j],carry,ans);
        }
        else if(i >= 0 && j < 0)
        {
            char c = ' ';
            add(a[i],c,carry,ans);
        }
        else
        {
            char c = ' ';
            add(c,b[j],carry,ans);
        }
        
        i--;
        j--;
        count++;
    }while(carry == '1' || i >= 0 || j >= 0);
     
    //cout << int(count) << endl;
    cout << ans << endl;

}
