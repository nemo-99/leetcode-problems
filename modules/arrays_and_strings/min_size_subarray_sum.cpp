#include <iostream>
#include <vector>
using namespace std;

/*
 * given an array of n +ve integers and a 
 * val s, find the smallest continuous subarray
 * whose sum is greator than s
 * nums = [2,3,1,2,4,3]
 * ans = 2
 */

int main()
{
    vector <int> nums = {2,3,1,2,4,3};
    if(nums.size() == 0)
        return 0;
    int s = 7;
    int i = 0,j = 0;
    int sum = nums[0];
    int len = 0,minlen = nums.size()+1;
    while(j < nums.size())
    {
        if(sum >= s)
        {
            len = j-i+1;
            if(len == 1)
            {
                cout << len << endl;
                return 1;
            }
            if(len < minlen)
            {
                minlen = len;
            }

            sum -= nums[i];
            i++;
         }
        else
        {
            j++;
            if(j == nums.size())
            {
                break;
            }
            if(minlen == nums.size()+1)
            {
                //j++;
                sum += nums[j];
            }
            else
            {
                sum -= nums[i];
                i++;
                //j++;
                sum += nums[j];
            }
        }
        cout << sum << " " << i << j << endl;
        if( i == j && j == nums.size()-1)
            break;
        
    }
    //int len = j-i;
    if(minlen == nums.size()+1)
    {
        minlen = 0;
    }
    cout << minlen << endl;

}
