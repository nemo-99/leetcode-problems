#include <iostream>
#include <vector>
using namespace std;

/*
 * Move all zeros in an array to the end
 * Input: [0,1,0,3,12]
 * Output: [1,3,12,0,0]
 */

int main()
{
    vector <int> nums = {0,1,0,3,12};
    int count = 0;
    for(int i = 0;i < nums.size();i++)
    {
        if(nums[i] == 0)
        {
            nums.erase(nums.begin()+i);
            i--;
            count++;
        }
    }

    for(int i = 0;i < count;i++)
    {
        nums.push_back(0);
    }

    for(int i = 0;i < nums.size();i++)
    {
        cout << nums[i];
    }
    cout << endl;


}
