#include <iostream>
#include <vector>
using namespace std;

/*
 * rotate nums array by k steps
 * nums = [1,2,3,4,5,6,7]
 * k = 3
 * ans = [5,6,7,1,2,3,4]
 */

int main()
{
    vector <int> nums = {1,2,3,4,5,6,7};
    int k = 3;
    int j = nums.size();
    while(k > 0)
    {
        int swap = nums.back();
        nums.pop_back();
        nums.emplace(nums.begin(),swap);
        k--;
    }

    for(int i = 0;i < nums.size();i++)
    {
        cout << nums[i] << endl;
    }
    
}
