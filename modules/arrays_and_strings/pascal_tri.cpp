#include <iostream>
#include <vector>
using namespace std;

/*
 * Print pascal's triangle for given numRows
 * for numRows = 4
 * 1
 * 11
 * 121
 * 1331
 */
int main()
{
    int numRows = 7;
    vector <vector <int>> ans;
    if(numRows == 0)
    {
      ans = {};
    }
    else
    {
        ans.push_back({1});
        int j = 1;
        while(j < numRows)
        {
            vector <int> buff;
            buff.push_back(1);
            for(int i = 0;i < ans[j-1].size()-1;i++)
            {
                buff.push_back(ans[j-1][i] + ans[j-1][i+1]);
            }
            buff.push_back(1);
            ans.push_back(buff);
            j++;
        }
    }
    for(int x = 0;x < ans.size();x++)
    {
        for(int y = 0; y < ans[x].size();y++)
        {
            cout << ans[x][y]; 
        }
        cout << endl;
    }
}
