#include <vector>
#include <iostream>
using namespace std;

/*
 * Given an array(nums), find an index(pivot index) such that
 * sum of elements to its left are equal to the sum of the
 * elements to its right
 *
 * */



int main()
{
    vector <int> nums = {1,7,3,6,5,6};
	if(nums.size() <=2 )
    {
		return -1;
    }
    
	int sum = 0;
	for(int i = 0;i < nums.size();i++)
	{
		sum += nums[i];
	}
	int left_sum = 0;
	int right_sum = 0;
	for(int i = 0;i < nums.size();i++)
	{
		right_sum = sum - left_sum - nums[i];
		if(left_sum == right_sum)
		{
		    return i;
		}
		left_sum += nums[i];
	}
	return -1;    
}
