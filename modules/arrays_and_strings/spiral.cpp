#include <iostream>
#include <vector>
using namespace std;

/*
 * Parse through matrix in spiral form
 * 123
 * 456 -> 123698745
 * 789
 */


int go_right(int &i, int &j,int &rc,int &cc,int m,int &co,vector <int> & a,vector <vector <int>> & ma)
{
    int k = 0;
    do
    {
        a.push_back(ma[i][j]);
        //cout << i << j << endl;
        j++;
        co++;
        k++;
    }
    while(k < cc && co < m);
    j--;
    i++;
    rc--;
    
    if(co == m)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

int go_left(int &i, int &j,int &rc,int &cc,int m,int &co,vector <int> & a,vector <vector <int>> & ma)
{
    int k = 0;
    do
    {
        a.push_back(ma[i][j]);
        //cout << i << j << endl;
        j--;
        co++;
        k++;
    }
    while(k < cc && co < m);
    j++;
    i--;
    rc--;
    if(co == m)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

int go_down(int &i, int &j,int &rc,int &cc,int m,int &co,vector <int> & a,vector <vector <int>> & ma)
{
    int k = 0;
    do
    {
        a.push_back(ma[i][j]);
        //cout << i << j << endl;
        i++;
        co++;
        k++;
    }
    while(k < rc && co < m);
    i--;
    j--;
    cc--;
    if(co == m)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

int go_up(int &i, int &j,int &rc,int &cc,int m,int &co,vector <int> & a,vector <vector <int>> & ma)
{
    int k = 0;
    do
    {
        a.push_back(ma[i][j]);
        //cout << i << j << endl;
        i--;
        co++;
        k++;
    }
    while(k < rc && co < m);
    i++;
    j++;
    cc--;
    if(co == m)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

int main()
{
    vector <vector <int>> matrix = {{1,2,3,4},{5,6,7,8},{9,10,11,12}};
    int i = 0;
    int j = 0;
    int rowcount = matrix.size();
    int colcount = matrix[0].size();
    vector <int> ans;
    int max = rowcount*colcount;
    int count = 0;
    //cout << max << endl;
    while(true)
    {
       int flag = 0;
       flag = go_right(i,j,rowcount,colcount,max,count,ans,matrix);
       if(flag == 1)
       {
           break;
       }
       flag = 0;
       flag = go_down(i,j,rowcount,colcount,max,count,ans,matrix);
       if(flag == 1)
       {
           break;
       }
       flag = 0;
       flag = go_left(i,j,rowcount,colcount,max,count,ans,matrix);
       if(flag == 1)
       {
           break;
       }
       flag = 0;
       flag = go_up(i,j,rowcount,colcount,max,count,ans,matrix);
       if(flag == 1)
       {
           break;
       }
       flag = 0;
       //cout << count << endl;

    }

    for(int i = 0;i < ans.size();i++)
    {
        cout << ans[i] << endl;
    }
}
