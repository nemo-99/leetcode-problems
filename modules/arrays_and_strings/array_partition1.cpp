#include <iostream>
#include <math.h>
#include <vector>
#include <algorithm>
using namespace std;

/*
 * Regroup array of 2n integers into n pairs
 * such that each sum of each min(a,b) is 
 * maximized
 *
 * nums = [7,0,6,4,9,6]
 * ans = 13
 */

bool mysort(int i,int j)
{
    return (i<j);
}
int main()
{
    vector <int> nums = {7,0,6,4,9,6};
    sort(nums.begin(),nums.end(),mysort);
    int sum = 0;
    for(int i = 0;i < nums.size();i+=2)
    {
        sum += nums[i];
    }
    cout << sum << endl;
    return sum;

}
