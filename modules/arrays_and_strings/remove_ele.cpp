#include <iostream>
#include <vector>
using namespace std;

/*
 * Remove all instances of a given val from 
 * nums array. Return the length of the new
 * reduced array. The contents of the array
 * beyond the returned length can be arbitrary
 *
 * nums = [0,1,2,2,3,0,4,2]
 * k = 5
 * nums = [0,1,3,0,4,0,4,2]
 */
int main()
{
    vector <int> nums = {0,1,2,2,3,0,4,2};
    int val = 2;
    int k = 0;

    for(int i = 0;i < nums.size();i++)
    {
        if(nums[i] != val)
        {
            nums[k] = nums[i];
            k++;
        }
    }

    for(int i = 0; i < nums.size();i++)
    {
        cout << nums[i];
    }
    cout << endl << k << endl;
    return k;
    

}
