#include <iostream>
using namespace std;

/*
 * Reverse the words in a string 
 * Input: "Let's take LeetCode contest"
 * Output: "s'teL ekat edoCteeL tsetnoc"
 */

int main()
{
    string s = "Let's take LeetCode contest";
    int startidx = 0;
    int endidx = 0;
    string ans = "";
    if(s == "")
    {
        ans = "";
    }
    else
    {
        for(int i = 0; i < s.length()-1;i++)
        {
            if((s[i] != ' ' && s[i+1] == ' ') || i+1 == s.length() - 1)
            {
                if( i+1 == s.length() - 1)
                {
                    startidx = i+1;
                }
                else
                {
                    startidx = i;
                }
                for(int j = startidx;j >= endidx;j--)
                {
                    ans += s[j];
                }
                endidx = i+2;
                if(i+1 != s.length() - 1)
                {
                    ans += ' ';
                }

            }
        }
    }

    cout << ans << endl;
}

