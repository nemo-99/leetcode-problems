#include <iostream>
#include <vector>
using namespace std;

/*
 * Reverse a character array in-place without using
 * an extra array
 */

int main()
{
    vector <char> s = {'h','e','l','l','o'};
    char swap;
    int i = 0;
    int j = s.size()-1;

    while(i < j)
    {
        swap = s[i];
        s[i] = s[j];
        s[j] = swap;
        i++;
        j--;
    }

    for(int k = 0;k < s.size();k++)
    {
        cout << s[k] << endl;
    }

}
