#include <iostream>
#include <vector>
using namespace std;

/*
 * find longest common prefix in a vector of strings
 * strs = {"flower","flow","flight"}
 * ans = "fl"
 */
int main()
{
    int idx = 0;
    vector <string> strs = {""};
    if(strs.size() == 0)
    {
        cout << "" << endl;
        return 0;
    }
    else if(strs.size() == 1)
    {
        cout <<  strs[0] << endl;
        return 0;
    }
    string ans = "";
    while(true)
    {
        for(int i = 0;i < strs.size()-1;i++)
        {
            if(idx == strs[i].length())
            {
                cout << ans << endl;
                return 0;
            }
            else if(strs[i][idx] == strs[i+1][idx])
            {
                continue;
            }
            else
            {
                cout << ans << endl;
                return 0;
            }


        }
        ans += strs[0][idx];
        idx++;
    }
}
