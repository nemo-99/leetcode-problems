#include <iostream>
#include <vector>
using namespace std;

/*
 * Print elements of a matrix in a diagonal order
 * 123
 * 456 -> 1,2,4,7,5,3,6,8,9
 * 789
 */


int main()
{
    vector <vector<int>> matrix = { {1,2,3},{4,5,6},{7,8,9}};
    int count = 0;
    int i = 0;
    int j = 0;
    int rownum = matrix.size();
    vector <int> ans;
    if(rownum == 0)
    {
        return 0;
    }
    int colnum = matrix[0].size();
    int num = rownum * colnum; 
    //cout << num << endl;
    while(count < num)
    {
        ans.push_back(matrix[i][j]);
        if((i+j) % 2 == 0)
        {
            i--;
            j++;
        }
        else
        {
            i++;
            j--;
        }
        if( (i < 0) && (j < colnum))
        {
            i++;
        }
        if( j >= colnum )
        {
            i+=2;
            j--;
        }
        if( i >= rownum)
        {
            j += 2;
            i--;
        }
        if(( j < 0) && (i < rownum ) )
        {
            j++;
        }
        //cout << i << j << endl; 
        count++;
    }
    
    for(int k = 0; k < ans.size();k++)
    {
        cout << ans[k] << endl;
    }
 
}
