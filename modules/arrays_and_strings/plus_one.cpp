#include <iostream>
#include <vector>
using namespace std;

/*
 * Add one to a number each of whose digits are represented
 * by an index in the array
 *
 */


int main()
{
    vector <int> nums = {9,9};
    int carry = 0;
    int pos = nums.size() - 1;
    do{
        if(nums[pos] == 9)
        {
            nums[pos] = 0;
            pos--;
            carry = 1;
        }
        else
        {
            nums[pos] += 1;
            carry = 0;
        }
        if(pos == -1)
        {
            nums.insert(nums.begin(),1);
            carry = 0;
        }
    }while(carry != 0);

    for(int i = 0;i < nums.size();i++)
    {
        cout << nums[i] << ",";
    }

    cout << endl;
}

