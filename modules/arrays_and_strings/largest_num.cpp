#include <iostream>
#include <vector>
using namespace std;

/*
 * Return Index of largest element if it is atleast
 * twice as large as the next largest element in 
 * the array else return -1
 *
 */


int main()
{
    vector <int> nums = {0,0,3,2};
    int largest = 0;
    int slargest = 0;
    int largeindex = -1;
    for(int i = 0;i < nums.size();i++)
    {
        if(nums[i] > largest)
        {
            slargest = largest;
            largest = nums[i];
            largeindex = i;
        }
        else if(nums[i] > slargest)
        {
            slargest = nums[i];
        }

    }

    if(largest >= 2*slargest)
    {
        cout << largeindex << endl;
    }
    else
    {
        cout <<  -1 << endl;
    }
}
