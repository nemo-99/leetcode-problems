#include <iostream>
#include <vector>
using namespace std;

/*
 * given a val rowIndex print the corresponding
 * row of the pascal triangle
 * rowIndex = 3
 * ans = 1331
 */

int main()
{
    int rowIndex = 4;
    if(rowIndex == 0)
    {
        cout << 1 << endl;
        return 0;
    }
    else if(rowIndex == 1)
    {
        cout << 11 << endl;
        return 0;
    }
    else
    {
        vector <int> ans = {1,1};
        int j = 0;
        while(j < rowIndex -1)
        {
            vector <int> newans = {1}; 
            for(int i = 0;i < ans.size()-1;i++)
            {
                int val = ans[i] + ans[i+1];
                newans.push_back(val);
            }
            newans.push_back(1);
            ans = newans; 
            j++;
        }

        for(int k = 0;k < ans.size();k++)
        {
            cout << ans[k]; 
        }
        cout << endl;
    }

}
