#include <iostream>
#include <vector>
using namespace std;

/*
 * Remove duplicates from a sorted array
 * Input: nums = [0,0,1,1,1,2,2,3,3,4]
 * Output: 5, nums = [0,1,2,3,4]
 */

int main()
{
    vector <int> nums = {0,0,1,1,1,2,2,3,3,4};
    if(nums.size() == 0)
    {
        return 0;
    }
    for(int i = 0;i < nums.size()-1;i++)
    {
        if(nums[i] == nums[i+1])
        {
            nums.erase(nums.begin()+i+1);
            i--;
        }
    }

    for(int i = 0; i < nums.size();i++)
    {
        cout << nums[i];
    }
    cout << endl;
    cout << "Length = " << nums.size() << endl;

}
