#include <iostream>
using namespace std;

/*
 * implement strstr() function
 * if occurence of needle string is found in haystack string
 * return index of where it is found 
 * if no occurence is found return -1
 * if needle is empty string return 0
 * haystack = "hello", needle = "ll"
 * ans = "2"
 */

int main()
{
    string haystack = "a";
    string needle = "a";
    int ans = -1;
    int flag = 0;
    for(int i = 0;i < haystack.length();i++)
    {
        if((haystack.length() - i ) < needle.length())
        {
            break;
        }
        if(haystack[i] == needle[0])
        {
            int j = i+1;
            int k = 1;
            while(k < needle.length() && j < haystack.length())
            {
                if(haystack[j] == needle[k])
                {
                    k++;
                    j++;
                }
                else
                {
                    break;
                }
            }
            if(k == needle.length())
            {
                ans = i;
                break;
            }
        }
    }
    if(needle.length() == 0)
    {
        ans = 0;
    }
    cout << ans << endl;
}
