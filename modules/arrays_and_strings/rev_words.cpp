#include <iostream>
using namespace std;

/*
 * reverse words in a string
 * s = "  hello   world "
 * ans = "world hello"
 * ignore starting,ending and middle whitespaces(if more than one)
 */
int main()
{
    string s = "  hello world  ";
    string ans = "";
    int startidx = 0;
    int endidx = 0;
    for(int i = s.length()-1;i > 0;i--)
    {
        if( s[i-1] != ' ' && s[i] == ' ') 
        {
            endidx = i;
            if(ans != "")
            {
                ans += " ";
            }
        }

        else if( i == s.length()-1 && i != ' ')
        {
            endidx = i+1;
        }
        
        if(( s[i] != ' ' && s[i-1] == ' ') )
        {
            startidx = i;
            for(int j = startidx;j < endidx;j++)
            {
               ans += s[j]; 
            }
        }

        else if(  i == 1 &&  s[i-1] != ' ')
        {
            startidx = i-1;
            for(int j = startidx;j < endidx;j++)
            {
               ans += s[j]; 
            }
        }

            
    }

    cout << ans << endl;

}
