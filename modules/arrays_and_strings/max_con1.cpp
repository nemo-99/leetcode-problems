#include <iostream>
#include <vector>
using namespace std;

/*
 * Find maximum number of consecutive 1s in 
 * an array
 * nums = [1,0,1,1,0,1]
 * ans = 2
 */

int main()
{
    vector < int> nums = {1,0,1,1,0,1};
    int k = 0;
    int maxk = 0;
    for(int i = 0;i < nums.size();i++)
    {
        if(nums[i] == 1)
        {
            k++;
            if(k > maxk)
            {
                maxk = k;
            }
        }
        else
        {
            k = 0;
        }
    }
    cout << maxk << endl;
    return maxk;
}
