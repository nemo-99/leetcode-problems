#include <iostream>
#include <vector>
using namespace std;

/*
 * for an array in sorted order, return indices
 * of two numbers whose sum is equal to a given 
 * target number
 * index1 < index2
 * there is exactly one solution for a given array
 */

int main()
{
    vector <int> numbers = {-1,0};
    int target = -1;
    int i = 0;
    int j = numbers.size() - 1;
    vector <int> answer; 
    while(i < j)
    {
        int sum = numbers[i] + numbers[j];
        if(sum == target)
        {
            answer.push_back(i+1);
            answer.push_back(j+1);
            cout << answer[0] << " " << answer[1] << endl;
            return 0;
        }
        else if(sum > target)
        {
            j--;
        }
        else
        {
            i++;
        }
    }

}
