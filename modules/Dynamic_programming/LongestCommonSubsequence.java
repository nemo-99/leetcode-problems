package modules.Dynamic_programming;

/*
    Given two strings, find the length of the longest common subsequence
    A subsequence is a new string generated from the original string while
    omitting zero or more elements while preserving the relative ordering

    Input: text1 = "abcde", text2 = "ace"
    Output: 3
*/

class LongestCommonSubSequence {

    int results[][];

    // Memoized Top-Down Solution
    public int longestCommonSubsequenceMemoized(String text1, String text2) {
        results = new int[text1.length()][text2.length()];
        return longestCommonSubsequenceMemoizedAux(text1.length() - 1, text2.length() - 1, text1, text2);
    }

    public int longestCommonSubsequenceMemoizedAux(int text1Idx, int text2Idx, String text1, String text2) {
        if (text1Idx == -1 || text2Idx == -1) {
            return 0;
        } else if (results[text1Idx][text2Idx] > 0) {
            return results[text1Idx][text2Idx];
        } else if (text1.charAt(text1Idx) == text2.charAt(text2Idx)) {
            results[text1Idx][text2Idx] = longestCommonSubsequenceMemoizedAux(text1Idx - 1, text2Idx - 1, text1, text2)
                    + 1;
        } else {
            results[text1Idx][text2Idx] = Math.max(
                    longestCommonSubsequenceMemoizedAux(text1Idx - 1, text2Idx, text1, text2),
                    longestCommonSubsequenceMemoizedAux(text1Idx, text2Idx - 1, text1, text2));
        }
        return results[text1Idx][text2Idx];
    }

    // Bottom - Up Solution
    public int longestCommonSubsequence(String text1, String text2) {
        int results[][] = new int[text1.length() + 1][text2.length() + 1];

        char[] text1Array = text1.toCharArray();
        char[] text2Array = text2.toCharArray();

        for (int i = 0; i < results.length; i++) {
            for (int j = 0; j < results[i].length; j++) {
                if (i == 0 || j == 0) {
                    results[i][j] = 0;
                } else if (text1Array[i - 1] == text2Array[j - 1]) {
                    results[i][j] = results[i - 1][j - 1] + 1;
                } else {
                    results[i][j] = results[i - 1][j] > results[i][j - 1] ? results[i - 1][j] : results[i][j - 1];
                }
            }
        }
        return results[text1.length()][text2.length()];
    }

    public static void main(String[] args) {
        LongestCommonSubSequence longestCommonSubSequence = new LongestCommonSubSequence();
        String text1 = "abcde";
        String text2 = "ace";
        // Uncomment below line to run using bottom-up solution
        // System.out.println(longestCommonSubSequence.longestCommonSubsequence(text1,
        // text2));
        System.out.println(longestCommonSubSequence.longestCommonSubsequenceMemoized(text1, text2));
    }
}