"""
    Given a string s and dictionary of strings wordDict,
    return true if s can be perfectly broken into the strings 
    given in wordDict
"""


def wordBreak(s, wordDict):
    def wordBreakAux(s, wordD, memo):
        if s in memo:
            return memo[s]

        for i in range(len(s)):
            firstPart = s[: i + 1]
            if firstPart in wordD:

                if len(firstPart) == len(s):
                    memo[s] = True
                    return True

                if wordBreakAux(s[i + 1 :], wordDict, memo) == True:
                    return True
                else:
                    memo[s[i + 1 :]] = False

        return False

    wordD = dict()
    memo = dict()
    for word in wordDict:
        wordD[word] = 1

    return wordBreakAux(s, wordD, memo)


if __name__ == "__main__":
    s = "applepenapple"
    wordDict = ["apple", "pen"]
    # wordDict = ["cats", "dog", "sand", "and", "cat"]
    print(wordBreak(s, wordDict))
