"""
    Given a string, return if it can be a palindrome if at most 
    character was deleted
"""


def validPalindrome(s):
    secondChance = False
    memo = {}

    def validPalindromeRecursive(s, secondChance, memo):
        i = 0
        j = len(s) - 1

        if s in memo:
            return memo[s]

        while i < j:
            if s[i] != s[j] and secondChance == False:
                if s[i] != s[j - 1] and s[i + 1] != s[j]:
                    memo[s] = False
                    return False

                secondChance = True

                return validPalindromeRecursive(
                    s[i + 1 : j + 1], secondChance, memo
                ) or validPalindromeRecursive(s[i:j], secondChance, memo)

            elif s[i] != s[j]:
                memo[s] = False
                return False

            i += 1
            j -= 1

        memo[s] = True
        return True

    return validPalindromeRecursive(s, secondChance, memo)


if __name__ == "__main__":
    s = "ebcbbececabbacecbbcbe"
    print(validPalindrome(s))
