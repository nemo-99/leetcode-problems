'''
    https://leetcode.com/problems/add-binary/
'''

def addBinary( a: str, b: str) -> str:
    aInt = int(a,2)
    bInt = int(b,2)
    sInt = aInt + bInt

    if sInt == 0:
        return '0'

    s = ''
    while sInt != 0:
        s += str(sInt % 2)
        sInt = sInt // 2
        print(sInt)
    
    return s[::-1]


if __name__ == '__main__':
    print(addBinary('11','1'))