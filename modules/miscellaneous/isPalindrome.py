"""
    Given a string, ignore all non-alphanumeric characters
    and return if the string is a palindrome or not
"""


def isPalindrome(s):
    i = 0
    j = len(s) - 1

    while j > i:
        if not str(s[i]).isalnum():
            i += 1
            continue

        if not str(s[j]).isalnum():
            j -= 1
            continue

        if str(s[i]).lower() != str(s[j]).lower():
            return False

        i += 1
        j -= 1

    return True


if __name__ == "__main__":
    s = "race a car"
    print(isPalindrome(s))
