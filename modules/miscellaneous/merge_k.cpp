#include <vector>
#include <algorithm>
using namespace std;

/*
 * Given a vector of k sorted lists
 * merge them into a single linked list
 */

struct ListNode {
      int val;
      ListNode *next;
      ListNode() : val(0), next(nullptr) {}
      ListNode(int x) : val(x), next(nullptr) {}
      ListNode(int x, ListNode *next) : val(x), next(next) {}
  };

ListNode* mergeKLists(vector<ListNode*>& lists)
{
    if(lists.empty())
    {
        return NULL;
    }

    vector <int> arr;
    for(ListNode * list : lists)
    {
        while(list != NULL)
        {
            arr.push_back(list->val);
            list = list->next;
        }
    }

    if(arr.empty())
    {
        return NULL;
    }

    sort(arr.begin(),arr.end());

    ListNode * ans = new ListNode(arr.front());
    ListNode * buffer = ans;

    for(int i = 1; i < arr.size();i++)
    {
        buffer->next = new ListNode(arr[i]);
        buffer = buffer->next;
    }

    return ans;
}

int main()
{
}
