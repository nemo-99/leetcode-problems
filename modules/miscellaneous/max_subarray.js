"use strict";

/*
 * Given an array nums, return
 * the maximum sum of a contiguous
 * subarray containing atleast one number
 * Input: nums = [-2,1,-3,4,-1,2,1,-5,4]
 * Output: 6
 */

var maxSubArray = function(nums) {
	let sum = -10e5;
	let maxSum = -10e5;
	for(let num of nums)
	{
		if(num > num + sum)
		{
			sum = num;
		}
		else
		{
			sum += num;
		}

		if(sum > maxSum)
		{
			maxSum = sum;
		}
	}

	return maxSum;
};


