'''
   https://leetcode.com/problems/sum-of-root-to-leaf-binary-numbers/ 
'''
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
    
def sumRootToLeaf(root) -> int:

    def auxFunction(root,total,encodedString):
        encodedString += str(root.val)

        if root.left is None and root.right is None:
            val = int(encodedString,2)
            total += val
            return total
        

        if root.right is not None:
            total = auxFunction(root.right,total,encodedString)
        
        if root.left is not None:
            total = auxFunction(root.left,total,encodedString)
        
        return total

    return auxFunction(root,0,'')