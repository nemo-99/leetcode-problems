/*
 * Given an int array candies and int extraCandies
 * figure out if there is a way to distribute extraCandies
 * to among kids such that each kid has the greatest number
 * of candies
 *
 * Input: candies = [2,3,5,1,3], extraCandies = 3
 * Output: [true,true,true,false,true] 
 */ 

import java.util.List;
import java.util.ArrayList;

class GreatestNumberOfCandies {
    public static void main(String[] args) {
        Solution solution = new Solution();
        int[] candies = { 4, 2, 1, 1, 2 };
        System.out.println(solution.kidsWithCandies(candies, 1));
    }
}

class Solution {
    public List<Boolean> kidsWithCandies(int[] candies, int extraCandies) {
        List<Boolean> candyList = new ArrayList<>();
        int max = -1;

        for (int candy : candies) {
            if (candy > max) {
                max = candy;
            }
        }

        for (int candy : candies) {
            if (candy + extraCandies >= max) {
                candyList.add(true);
            } else {
                candyList.add(false);
            }
        }

        return candyList;
    }
}
