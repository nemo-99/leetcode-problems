def minCostClimbingStairs(cost: list[int]) -> int:
    def auxFunction(arr,index,memo = {}):
        if index in memo:
            return memo[index]

        if index + 2 < len(arr):
            memo[index] = arr[index] + min(auxFunction(arr,index + 1,memo),auxFunction(arr,index + 2,memo))
            return memo[index]
        elif index + 1 < len(arr):
            memo[index] = arr[index]
            return memo[index]
        else:
            memo[index] = arr[index]
            return memo[index]

    memo = dict()
    return min(auxFunction(cost,0,memo),auxFunction(cost,1,memo))


if __name__ == '__main__':
    cost = [1,100,1,1,1,100,1,1,100,1]
    print(minCostClimbingStairs(cost))