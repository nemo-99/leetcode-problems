'''
https://leetcode.com/problems/set-matrix-zeroes/
'''


def setZeroes(matrix: list[list[int]]) -> None:
    isCol = False
    for i in range(len(matrix)):
        if matrix[i][0] == 0:
            isCol = True
        for j in range(len(matrix[0])):
            if matrix[i][j] == 0:
                matrix[i][0] = 0
                if j != 0:
                    matrix[0][j] = 0

    for i in range(1,len(matrix)):
        for j in range(1,len(matrix[0])):
            if matrix[i][0] == 0 or matrix[0][j] == 0:
                matrix[i][j] = 0

    if matrix[0][0] == 0:
        for i in range(len(matrix[0])):
            matrix[0][i] = 0


    if isCol:
        for i in range(len(matrix)):
            matrix[i][0] = 0


if __name__ == '__main__':
    matrix = [[1,2,3,4],[5,0,7,8],[0,10,11,12],[13,14,15,0]]
    setZeroes(matrix)
    print(matrix)