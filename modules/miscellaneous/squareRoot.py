'''
https://leetcode.com/problems/sqrtx/
'''


def mySqrt(x: int) -> int:
    # Linear time solution
    """
    if x == 0:
        return 0
    i = 1
    for i in range(1,x//2 + 1):
        if i * i > x:
            return i - 1

    return i
    """

    # Logarithmic time solution

    if x == 1:
        return 1

    low = 0
    high = x // 2
    mid = (high + low) // 2

    while high >= low:

        if mid * mid > x:
            high = mid - 1
        elif mid * mid < x:
            low = mid + 1
        else:
            return mid

        mid = (high + low) // 2

    return mid


if __name__ == "__main__":
    print(mySqrt(1))
