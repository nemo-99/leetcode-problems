/*
 * Given an array with elements [x1,x2..xn,y1,y2...yn]
 * and integer n, shuffle the array such that
 * the new array is equal to [x1,y1,x2,y2...xn,yn]
 */

class ShuffleTheArray {
    public static void main(String[] args) {
        int nums[] = { 2, 5, 1, 3, 4, 7 };
        int n = 3;
        for (int i : new Solution().shuffle(nums, n)) {
            System.out.println(i);
        }
    }
}

class Solution {
    public int[] shuffle(int[] nums, int n) {
        int[] answer = new int[2 * n];

        for (int i = 0; i < n; i++) {
            answer[i * 2] = nums[i];
        }

        int k = 1;
        for (int i = n; i < n * 2; i++) {
            answer[k] = nums[i];
            k += 2;
        }

        return answer;

    }
}
