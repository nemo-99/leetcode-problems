'''
    https://leetcode.com/problems/baseball-game/
'''

def calPoints(ops):
    ans = []
    
    for op in ops:
        if op == '+':
            calSum = ans[len(ans) - 1] + ans[len(ans) - 2]
            ans.append(calSum)
        elif op == 'D':
            ans.append(ans[len(ans)-1]*2)
        elif op == 'C':
            ans.pop()
        else:
            ans.append(int(op))
    
    return sum(ans)

