'''
https://leetcode.com/problems/find-a-corresponding-node-of-a-binary-tree-in-a-clone-of-that-tree/
'''
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

def getTargetCopy(original: TreeNode, cloned: TreeNode, target: TreeNode) -> TreeNode:
    if original is target:
        return cloned
    else:
        tmpNode = None
        if original.left is not None:
            tmpNode = getTargetCopy(original.left,cloned.left,target)

        if tmpNode is None and original.right is not None:
            tmpNode = getTargetCopy(original.right,cloned.right,target)

        return tmpNode
