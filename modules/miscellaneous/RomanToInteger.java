import java.util.HashMap;

/*
    Roman to Integer 
    Given a string of roman numerals, return the integer value
*/

public class RomanToInteger {
    public int romanToInt(String s) {
        HashMap<Character, Integer> romanToInteger = new HashMap<>();
        romanToInteger.put('I', 1);
        romanToInteger.put('V', 5);
        romanToInteger.put('X', 10);
        romanToInteger.put('L', 50);
        romanToInteger.put('C', 100);
        romanToInteger.put('D', 500);
        romanToInteger.put('M', 1000);

        int integerAnswer = 0;

        for (int i = 0; i < s.length() - 1; i++) {
            if (romanToInteger.get(s.charAt(i)) < romanToInteger.get(s.charAt(i + 1))) {
                integerAnswer -= romanToInteger.get(s.charAt(i));
            } else {
                integerAnswer += romanToInteger.get(s.charAt(i));
            }
        }
        return integerAnswer + romanToInteger.get(s.charAt(s.length() - 1));
    }

}
