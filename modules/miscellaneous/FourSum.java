
/*
Given an array of integers and a target,
find distinct combinations of integers from the array
such that their sum is equal to the target
*/

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class FourSum {
    public List<List<Integer>> fourSum(int[] nums, int target) {

        Arrays.sort(nums);
        ArrayList<List<Integer>> answerIndices = new ArrayList<List<Integer>>();
        HashSet<List<Integer>> answers = new HashSet<>();

        for (int a = 0; a < nums.length - 3; a++) {
            for (int b = a + 1; b < nums.length - 2; b++) {
                int leftIndex = b + 1;
                int rightIndex = nums.length - 1;

                while (rightIndex > leftIndex) {
                    int sum = nums[a] + nums[b] + nums[leftIndex] + nums[rightIndex];
                    if (sum == target) {
                        if (answers.contains(Arrays.asList(nums[a], nums[b], nums[leftIndex], nums[rightIndex]))) {
                            leftIndex++;
                            rightIndex--;
                        } else {
                            answerIndices.add(Arrays.asList(nums[a], nums[b], nums[leftIndex], nums[rightIndex]));
                            answers.add(Arrays.asList(nums[a], nums[b], nums[leftIndex], nums[rightIndex]));
                        }
                    } else if (sum < target) {
                        leftIndex++;
                    } else {
                        rightIndex--;
                    }
                }
            }
        }

        return answerIndices;

    }

    public static void main(String[] args) {

        FourSum fourSum = new FourSum();
        int nums[] = { 2, 2, 2, 2, 2 };
        System.out.println(fourSum.fourSum(nums, 8));
    }
}
