"""
    Given two arrays, num1 and num2 , merge num2 into num1
"""


def merge(nums1, m, nums2, n):
    tmpArr = []
    i = 0
    j = 0

    while i < m and j < n:
        if nums1[i] < nums2[j]:
            tmpArr.append(nums1[i])
            i += 1
        else:
            tmpArr.append(nums2[j])
            j += 1

    while i < m:
        tmpArr.append(nums1[i])
        i += 1

    while j < n:
        tmpArr.append(nums2[j])
        j += 1

    for i in range(m + n):
        nums1[i] = tmpArr[i]


if __name__ == "__main__":
    nums1 = [-1, 3, 0, 0, 0, 0, 0]
    nums2 = [0, 0, 1, 2, 3]
    merge(nums1, 2, nums2, len(nums2))
    print(nums1)
    """
   [-1,0,0,3,3,3,0,0,0]
    6
    [1,2,2]
    3 
    """
    """
    [-1,3,0,0,0,0,0]
    2
    [0,0,1,2,3]
    5
    """
