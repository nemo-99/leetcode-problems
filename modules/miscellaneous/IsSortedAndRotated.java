/*
    Check if Array is sorted and rotated
    given an array nums, return true if array was sorted and then rotated
    Duplicate values are allowed
    Input: nums = [3,4,5,1,2]
    Output: true
*/

class IsSortedAndRotated {

    public boolean check(int[] nums) {

        int count = 0;

        for (int i = 0; i < nums.length; i++) {
            if (nums[i] > nums[(i + 1) % nums.length]) {
                count++;
            }
        }

        if (count > 1) {
            return false;
        } else {
            return true;
        }
    }

    public static void main(String[] args) {
        IsSortedAndRotated isSortedAndRotated = new IsSortedAndRotated();
        int[] nums = { 7, 9, 1, 1, 1, 3 };
        System.out.println(isSortedAndRotated.check(nums));
    }
}
