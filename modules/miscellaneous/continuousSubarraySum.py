'''
 Continuous Array Sum 
 https://leetcode.com/problems/continuous-subarray-sum/
'''

def checkSubarraySum(nums, k):
    remDict = dict()

    cumSum = 0

    for i in range(len(nums)):
        cumSum += nums[i]

        rem = cumSum % k

        if rem in remDict:
            prevIndex = remDict[rem]
            if i - prevIndex > 1:
                return True
        else:
            remDict[rem] = i
        
        if rem == 0 and i > 0:
            return True
        
    return False
         


if __name__ == '__main__':
    nums = [23, 2, 6, 4, 7]
    k = 13
    print(checkSubarraySum(nums, k))
