/*
 * Defanging an IP address
 *
 * Input: address = "1.1.1.1"
 * Output: "1[.]1[.]1[.]1"
 */

class DefangIP {
    public static void main(String[] args) {
        Solution solution = new Solution();
        String ans = solution.defangIPaddr("255.100.50.0");
        System.out.println(ans);
    }
}

class Solution {
    public String defangIPaddr(String address) {
        String ans = "";

        for (int i = 0; i < address.length(); i++) {
            if (Character.isDigit(address.charAt(i))) {
                ans += address.charAt(i);
            } else {
                ans += "[.]";
            }
        }

        return ans;
    }
}
