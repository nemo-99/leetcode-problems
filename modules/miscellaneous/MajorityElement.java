package modules.miscellaneous;

import java.util.HashMap;

/*
    Given an array of integers, return the majority element.
    A majority element is one whose frequency count > floor(n/2)

    Input: nums = [2,2,1,1,1,2,2]
    Output: 2
*/

public class MajorityElement {

    // Done using hashMap in linear time
    public int majorityElement(int[] nums) {
        HashMap<Integer, Integer> numberCount = new HashMap<>();

        for (int i : nums) {

            if (numberCount.containsKey(i)) {
                numberCount.put(i, numberCount.get(i) + 1);
            } else {
                numberCount.put(i, 1);
            }

            if (numberCount.get(i) > nums.length / 2) {
                return i;
            }
        }

        return 0;

    }

    // Boyer-Moore voting algorithm - review !!
    public int majorityElementLinearSpace(int[] nums) {
        int count = 0;
        int candidate = -1;

        for (int i : nums) {
            if (count == 0) {
                candidate = i;
            }
            if (i == candidate) {
                count++;
            } else {
                count--;
            }
        }

        return candidate;
    }

    public static void main(String[] args) {

    }
}
