/*
 * Given an array nums, return running sum
 * of the array.
 * Input: nums = [1,2,3,4]
 * Output: [1,3,6,10]
 */

class RunningSum {
    public static void main(String[] args) {
        Solution solution = new Solution();
        int[] nums = { 1, 3, 5, 6 };
        int[] ans = solution.runningSum(nums);

        for (int i : ans) {
            System.out.println(i);
        }
    }
}

class Solution {
    public int[] runningSum(int[] nums) {
        int ans[] = new int[nums.length];

        ans[0] = nums[0];

        for (int i = 1; i < ans.length; i++) {
            ans[i] = ans[i - 1] + nums[i];
        }

        return ans;
    }
}
