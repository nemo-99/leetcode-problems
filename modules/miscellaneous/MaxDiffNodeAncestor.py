'''
    https://leetcode.com/problems/maximum-difference-between-node-and-ancestor/
'''


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

# N^2 time solution
'''
def maxAncestorDiff(self, root ) -> int:

    def getMax(root = root,ancestors = (),max = -1):
        for ancestor in ancestors:
            if max < abs(ancestor.val - root.val):
                max = abs(ancestor.val - root.val)
        
        ancestors = ancestors + (root,)
        
        if root.left != None:
            max = getMax(root.left,ancestors,max)
        if root.right != None:
            max = getMax(root.right,ancestors,max)
        
        return max

         
    
    return getMax()
'''


# Linear time solution
def maxAncestorDiff(self, root ) -> int:

    def getMax(root = root,minAncestor = 100001,maxAncestor = -1,maxVal = -1):

        diff = abs(root.val - minAncestor.val)
        if diff > maxVal:
            maxVal = diff

        diff = abs(root.val - maxAncestor.val)
        if diff > maxVal:
            maxVal = diff
          
        if root.val < minAncestor:
            minAncestor = root.val

        if root.val > maxAncestor:
            maxAncestor = root.val 
        
        if root.left != None:
            maxVal = getMax(root.left,minAncestor,maxAncestor,maxVal)
        if root.right != None:
            maxVal = getMax(root.right,minAncestor,maxAncestor,maxVal)
        
        return maxVal

         
    
    return getMax()
