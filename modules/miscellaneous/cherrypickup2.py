'''
   https://leetcode.com/problems/cherry-pickup-ii/ 
'''

def cherrypickup(grid):
    def getMoves(position, grid):
        newPositions = [
            (position[0] + 1, position[1] - 1),
            (position[0] + 1, position[1]),
            (position[0] + 1, position[1] + 1),
        ]

        finalNewPositions = []

        for newPosition in newPositions:
            if not (
                newPosition[0] == len(grid)
                or newPosition[0] < 0
                or newPosition[1] == len(grid[0])
                or newPosition[1] < 0
            ):
                finalNewPositions.append(newPosition)

        return finalNewPositions

    def auxFunction(grid, robot1Position, robot2Position, memo):
        if (robot1Position, robot2Position) in memo:
            return memo[(robot1Position, robot2Position)]

        total = 0

        if robot1Position == robot2Position:
            total += grid[robot2Position[0]][robot2Position[1]]
        else:
            total += grid[robot2Position[0]][robot2Position[1]]
            total += grid[robot1Position[0]][robot1Position[1]]

        robot1Moves = getMoves(robot1Position, grid)
        robot2Moves = getMoves(robot2Position, grid)

        maxTotal = total

        for robot1Move in robot1Moves:
            for robot2Move in robot2Moves:
                tmpTotal = total + auxFunction(grid, robot1Move, robot2Move, memo)
                if tmpTotal > maxTotal:
                    maxTotal = tmpTotal

        memo[(robot1Position, robot2Position)] = maxTotal

        return maxTotal

    return auxFunction(grid, (0, 0), (0, len(grid[0]) - 1), {})


if __name__ == "__main__":
    grid = [[3, 1, 1], [2, 5, 1], [1, 5, 5], [2, 1, 1]]
    print(cherrypickup(grid))
