
'''
    https://leetcode.com/problems/count-and-say/
'''

def countAndSay(n: int) -> str:
    if n == 1:
        return "1"
    else:
        string = countAndSay(n - 1)
        freq = 1
        num = string[0]
        ans = ""

        for i in range(1, len(string)):
            if string[i] == num:
                freq += 1
            else:
                ans += str(freq) + num
                freq = 1
                num = string[min(len(string) - 1, i)]

        ans += str(freq) + num

        return ans



if __name__ == "__main__":
    print(countAndSay(4))
