'''
    Given a paragraph of words, return the most frequent words while excluding the
    words in banned list and also ignoring punctuations and returning everthing in
    lowercase
'''

import re


def mostCommonWord(paragraph: str, banned) -> str:
    bannedDict = {}
            
    for bannedWord in banned:
        bannedDict[bannedWord.lower()] = 1
    
    maxFreq = 0
    maxFreqWord = ''
    wordFreqs = dict()
    paragraphWords = re.split(" |\!|\?|'|,|;|\.",paragraph)

    for word in paragraphWords:
        word = word.lower()
        # word = word.strip("!?',;.")
        if len(word) == 0:
            continue
        
        if word in bannedDict:
            continue
        
        if word not in wordFreqs:
            wordFreqs[word] = 1
        else:
            wordFreqs[word] += 1
        
        if maxFreq < wordFreqs[word]:
            maxFreq = wordFreqs[word]
            maxFreqWord = word

    return maxFreqWord


if __name__ == '__main__':
    print(mostCommonWord("a, a, a, a, b,b,b,c, c",["a"]))
    # "a, a, a, a, b,b,b,c, c"
    # ["a"]