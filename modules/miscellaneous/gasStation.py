'''
    https://leetcode.com/problems/gas-station/
'''

def canCompleteCircuit(gas: list[int], cost: list[int]) -> int:
    totalCapacity = 0
    capacity = 0
    soln = 0

    for i in range(len(cost)):
        totalCapacity += gas[i] - cost[i]
        capacity += gas[i] - cost[i]

        if capacity < 0:
            capacity = 0
            soln = i + 1

    if totalCapacity < 0:
        return -1
    else:
        return soln


if __name__ == "__main__":
    gas = [5,8,2,8]
    cost = [6,5,6,6]
    print(canCompleteCircuit(gas, cost))
