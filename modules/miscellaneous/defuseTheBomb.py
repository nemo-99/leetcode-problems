'''
Given an array of integers code and a key k
if k > 0 : replace every element with the sum of next k integers
if k < 0 : replace every element with the sum of previous k integers
if k = 0 : replace every element with 0
Input: code = [5,7,1,4], k = 3
Output: [12,10,16,13]

'''


class Solution:
    def nextElement(self, code: list[int], i: int, k: int) -> int:
        if k > 0:
            return (i+1) % len(code)
        elif k < 0:
            if i - 1 < 0:
                return i - 1 + len(code)
            else:
                return i - 1

    def decrypt(self, code: list[int], k: int) -> list[int]:

        ans = []

        if k == 0:
            for i in range(len(code)):
                ans.append(0)
            return ans

        for i in range(0, len(code)):
            j = 0
            l = self.nextElement(code, i, k)
            sum = 0

            while j < abs(k):
                j += 1
                sum += code[l]
                l = self.nextElement(code, l, k)

            ans.append(sum)

        return ans


code = [2, 4, 9, 3]
k = -2
sol = Solution()
print(sol.decrypt(code, k))
