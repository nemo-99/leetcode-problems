'''
    https://leetcode.com/problems/can-place-flowers/submissions/
'''

def canPlaceFlowers(flowerbed: list[int], n: int) -> bool:
    if len(flowerbed) == 1:
        return (flowerbed[0] == 0 and n <= 1) or (flowerbed[0] == 1 and n == 0)

    for i in range(len(flowerbed)):
        if flowerbed[i] != 0:
            continue

        if i == 0:
            if flowerbed[i + 1] == 0:
                flowerbed[i] = 1
                n -= 1
        elif i == len(flowerbed) - 1:
            if flowerbed[i - 1] == 0:
                flowerbed[i] = 1
                n -= 1
        else:
            if flowerbed[i + 1] == 0 and flowerbed[i - 1] == 0:
                flowerbed[i] = 1
                n -= 1
        
        if n == 0:
            return True


    return n <= 0


if __name__ == "__main__":
    flowerbed = [1]
    n = 0
    print(canPlaceFlowers(flowerbed, n))
