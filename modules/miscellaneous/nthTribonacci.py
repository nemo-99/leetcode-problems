'''
https://leetcode.com/problemset/all/?page=1&search=delete+and+earn
'''


def tribonacci( n: int) -> int:
    arr = []
    arr.append(0)
    arr.append(1)
    arr.append(1)

    for i in range(3,n + 1):
        arr.append(arr[i-1] + arr[i-2]+ arr[i-3])

    return arr[n]


if __name__ == '__main__':
    print(tribonacci(4))
