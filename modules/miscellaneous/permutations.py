'''
    https://leetcode.com/problems/permutations/
'''


def permute(nums: list[int]) -> list[list[int]]:
    if len(nums) == 1:
        return [[nums[0]]]

    mutations = permute(nums[1:])

    ans = []
    for mutation in mutations:
        for i in range(len(mutation)):
            tmpMutation = mutation[:]
            tmpMutation.insert(i,nums[0])
            ans.append(tmpMutation)

        mutation.append(nums[0])
        ans.append(mutation)

    return ans


if __name__ == '__main__':
    print(permute([1,2,3]))