'''
    https://leetcode.com/problems/find-all-anagrams-in-a-string/
'''

def findAnagrams( s: str, p: str) -> list[int]:
    if len(p) > len(s):
        return []

    zipfDict = {}

    for char in p:
        if char in zipfDict:
            zipfDict[char] += 1
        else:
            zipfDict[char] = 1

    for i in range(len(p)):
        if s[i] in zipfDict:
            zipfDict[s[i]] -= 1
        else:
            zipfDict[s[i]] = -1

    ans = []

    for i in range(len(s) - len(p) + 1):
        flag = False
        for key in zipfDict.keys():
            if zipfDict[key] != 0:
                flag = True

        if not flag:
            ans.append(i)

        zipfDict[s[i]] += 1

        if i + len(p) >= len(s):
            continue

        if s[i + len(p)] in zipfDict:
            zipfDict[s[i + len(p)]] -= 1
        else:
            zipfDict[s[i + len(p)]] = -1

    return ans



if __name__ == '__main__':
    s = 'aa'
    p = 'bb'

    print(findAnagrams(s,p))