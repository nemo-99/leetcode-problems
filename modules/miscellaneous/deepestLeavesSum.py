'''
https://leetcode.com/problems/deepest-leaves-sum/
'''
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def deepestLeavesSum(root: list[TreeNode]) -> int:

    def getSum(root,currDepth):
        if root.left is None and root.right is None:
            return root.val,currDepth

        elif root.left is not None and root.right is not None:
            resultLeft = getSum(root.left,currDepth + 1)
            resultRight = getSum(root.right,currDepth + 1)

            if resultLeft[1] == resultRight[1]:
                return resultLeft[0] + resultRight[0],resultLeft[1]
            else:
                return resultLeft if resultRight[1] < resultLeft[1] else resultRight
        elif root.left is not None:
            return getSum(root.left,currDepth + 1)
        else:
            return getSum(root.right,currDepth + 1)

    return getSum(root,0)[0]
