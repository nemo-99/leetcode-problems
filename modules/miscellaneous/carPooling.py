'''
    https://leetcode.com/problems/car-pooling/
'''

import heapq

def carPooling(trips, capacity) -> bool:
    tripQ = []

    for trip in trips:
        heapq.heappush(tripQ,(trip[1],1,trip[0]))
        heapq.heappush(tripQ,(trip[2],0,trip[0]))
    
    tmpCapacity = 0
    while len(tripQ) != 0:
        payload = heapq.heappop(tripQ)
        if payload[1] == 1:
            tmpCapacity += payload[2]
        else:
            tmpCapacity -= payload[2]

        if tmpCapacity > capacity:
            return False
    
    
    return True

if __name__ == '__main__':
    trips = [[2,1,5],[3,3,7]] 
    capacity = 4
    print(carPooling(trips,capacity))
        


