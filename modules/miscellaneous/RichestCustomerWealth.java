/*
 * given an m*n grid of accounts where the 
 * ith row symbolizes the amount of funds 
 * stored by a single individual in several banks,
 * find out the customer with the maximum Wealth
 * Input: accounts = [[1,2,3],[3,2,1]]
 * Output: 6
 */ 


class RichestCustomerWealth {
    public static void main(String[] args) {
        int[][] array = { { 1, 2, 3 }, { 1, 1, 3 } };
        System.out.println(new Solution().maximumWealth(array));
    }
}

class Solution {
    public int maximumWealth(int[][] accounts) {
        int maxSum = 0;
        for (int i = 0; i < accounts.length; i++) {
            int sum = 0;
            for (int j = 0; j < accounts[i].length; j++) {
                sum += accounts[i][j];
            }
            if (sum > maxSum) {
                maxSum = sum;
            }
        }
        return maxSum;
    }
}
