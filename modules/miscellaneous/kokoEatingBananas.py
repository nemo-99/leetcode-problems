'''
    https://leetcode.com/problems/koko-eating-bananas/submissions/
'''
from math import ceil


def minEatingSpeed( piles: list[int], h: int) -> int:

    def checkIfSuff(piles,k,h):
        hourSum = 0
        for pile in piles:
            hourSum += ceil(pile/k)

        return hourSum <= h

    low = 1
    high = max(piles)

    mid = (low + high) // 2

    while high > low:

        if checkIfSuff(piles,mid,h):
            high = mid
        else:
            low = mid + 1

        mid = (low + high) // 2

    return mid













if __name__ == '__main__':
    piles = [30,11,23,4,20]
    h = 5

    print(minEatingSpeed(piles,h))