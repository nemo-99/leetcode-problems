'''
    Given a list consisting of intervals, return list 
    after merging all possible intervals
'''

def merge(intervals):
    sortedIntervals = sorted(intervals)

    ans = [sortedIntervals[0]]

    for i in range(1,len(intervals)):
        interval1 = ans[len(ans)-1]
        interval2 = sortedIntervals[i]

        if interval1[1] >= interval2[0]:
            newInterval = []
            newInterval.append(interval1[0])
            newInterval.append(max(interval2[1],interval1[1]))
            ans.pop()
            ans.append(newInterval)
        else:
            ans.append(interval2)
    
    return ans


if __name__ == '__main__':
    intervals = [[1,4],[0,1]]
    print(merge(intervals))
