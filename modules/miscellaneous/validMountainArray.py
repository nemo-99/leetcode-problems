"""
https://leetcode.com/problems/valid-mountain-array/
"""


def validMountainArray(arr: list[int]) -> bool:
    if len(arr) < 3:
        return False

    flag = False
    count1 = 0
    count2 = 0
    for i in range(len(arr) - 1):
        if arr[i] == arr[i + 1]:
            return False

        if not flag:
            if arr[i] > arr[i + 1]:
                flag = True
                count2 += 1
            else:
                count1 += 1

        else:
            if arr[i] < arr[i + 1]:
                return False
            else:
                count2 += 1

    return True and count1 != 0 and count2 != 0


if __name__ == "__main__":
    arr = [1, 3, 2]
    print(validMountainArray(arr))
