'''
    https://leetcode.com/problems/count-elements-with-strictly-smaller-and-greater-elements/
'''


def countElements(self, nums: list[int]) -> int:

    minElement = min(nums)
    maxElement = max(nums)
    count = 0

    for num in nums:
        if num > minElement and num < maxElement:
            count += 1

    return count
