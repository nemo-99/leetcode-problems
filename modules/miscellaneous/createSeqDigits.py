"""
    https://leetcode.com/problems/sequential-digits/submissions/
"""


def sequentialDigits(low: int, high: int) -> list[int]:
    def createSeqDigits(numDigits):
        nums = []
        for i in range(1, 9 - numDigits + 2):
            power = numDigits - 1

            j = i
            num = 0
            while power >= 0:
                num += j * (10 ** power)
                power -= 1
                j += 1

            nums.append(num)
        return nums

    lowStr = str(low)
    highStr = str(high)

    lowNumDigits = len(lowStr)
    highNumDigits = len(highStr)

    ans = []

    for i in range(lowNumDigits, highNumDigits + 1):
        seqDigits = createSeqDigits(i)
        for seqDigit in seqDigits:
            if seqDigit >= low and seqDigit <= high:
                ans.append(seqDigit)

    return ans


if __name__ == "__main__":
    low = 1000
    high = 13000
    print(sequentialDigits(low, high))
