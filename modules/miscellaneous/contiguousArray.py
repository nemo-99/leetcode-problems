'''
    https://leetcode.com/problems/contiguous-array/submissions/
'''


'''
def findMaxLength(nums: list) -> int:

    # DP solution - slow
    def auxFunction(nums,i,j,oneCount,zeroCount,memo={}):
        if (i,j) in memo:
            return memo[(i,j)]
        elif i == j:
            return 0
        elif oneCount == zeroCount:
            return j - i + 1
        else:
            tmpZeroCount = zeroCount
            tmpOneConut = oneCount
            if nums[j] == 0:
                tmpZeroCount -= 1
            else:
                tmpOneConut -= 1

            jResult = auxFunction(nums,i, j - 1,tmpOneConut,tmpZeroCount)

            memo[(i,j-1)] = jResult

            tmpZeroCount = zeroCount
            tmpOneConut = oneCount

            if nums[i] == 0:
                tmpZeroCount -= 1
            else:
                tmpOneConut -= 1

            iResult = auxFunction(nums,i + 1,j,tmpOneConut,tmpZeroCount)
            memo[(i+1,j)] = iResult
            return max(jResult,iResult)


    oneCount = 0
    zeroCount = 0

    for num in nums:
        if num == 0:
            zeroCount += 1
        else:
            oneCount += 1

    return auxFunction(nums,0,len(nums) - 1,oneCount,zeroCount)
'''

def findMaxLength(nums):
    count = 0
    map = {}
    maxDiff = 0

    for i in range(len(nums)):
        if nums[i] == 0:
            count -= 1
        else:
            count += 1

        if count == 0 and (i + 1) > maxDiff:
            maxDiff = i + 1


        if count in map:
            diff = i - map[count]
            if diff > maxDiff:
                maxDiff = diff
        else:
            map[count] = i

    return maxDiff






if __name__ == '__main__':
    nums = [0,1,0]
    print(findMaxLength(nums))
