'''
    Given an n x n matrix, rotate matrix in place by 90 degrees
'''
def rotate(matrix):
    coeff1 = 0
    coeff2 = 0
    startRow = 0
    startCol = 0

    while coeff1 < len(matrix):
        for _ in range(len(matrix) - 1 - coeff1):
            prevVal = matrix[startRow][startCol]

            i = startRow
            j = startCol + 1 

            while j < len(matrix) - coeff2:
                tmpVal = matrix[i][j]  
                matrix[i][j] = prevVal
                prevVal = tmpVal
                j += 1

            j -= 1
            i += 1

            while i < len(matrix) - coeff2:
                tmpVal = matrix[i][j]  
                matrix[i][j] = prevVal
                prevVal = tmpVal
                i += 1

            i -= 1
            j -= 1

            while j >= 0 + coeff2:
                tmpVal = matrix[i][j]  
                matrix[i][j] = prevVal
                prevVal = tmpVal
                j -= 1
            
            j += 1
            i -= 1


            while i >= 0 + coeff2:
                tmpVal = matrix[i][j]  
                matrix[i][j] = prevVal
                prevVal = tmpVal
                i -= 1

            i += 1
        
        coeff1 += 2
        coeff2 += 1
        startRow += 1
        startCol += 1

if __name__ == '__main__':
    # matrix = [[1,2,3,4],[5,6,7,8],[9,10,11,12],[13,14,15,16]]
    matrix = [[2,29,20,26,16,28],[12,27,9,25,13,21],[32,33,32,2,28,14],[13,14,32,27,22,26],[33,1,20,7,21,7],[4,24,1,6,32,34]]
    # matrix = [[1]]
    rotate(matrix,0,0)
    print(matrix)
