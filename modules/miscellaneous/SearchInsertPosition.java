/*
    Given a sorted array and a target integer, find 
    the target if it exists or find the position at 
    which the target should be at.
*/

class SearchInsertPosition {
    public int searchInsert(int[] nums, int target) {
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] >= target) {
                return i;
            }

        }
        return nums.length;
    }
}