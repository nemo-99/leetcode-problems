'''
    https://leetcode.com/problems/robot-bounded-in-circle/
'''

def isRobotBounded(instructions: str) -> bool:
    orientation = "N"
    position = (0, 0)
    leftChangeOrientation = {"N": "W", "W": "S", "S": "E", "E": "N"}
    rightChangeOrientation = {"N": "E", "W": "N", "S": "W", "E": "S"}
    suffixOrientation = {"N": (0, 1), "S": (0, -1), "E": (1, 0), "W": (-1, 0)}
    i = 0

    for instruction in instructions:
        if instruction == "G":
            position = (
                position[0] + suffixOrientation[orientation][0],
                position[1] + suffixOrientation[orientation][1],
            )
        elif instruction == "L":
            orientation = leftChangeOrientation[orientation]
            i = (i + 1) % 4
        else:
            orientation = rightChangeOrientation[orientation]
            i = (i + 3) % 4
        
        print(position)

    return i != 0 or (position[0] == 0 and position[1] == 0)


if __name__ == "__main__":
    print(isRobotBounded("RRGRRGLLLRLGGLGLLGRLRLGLRLRRGLGGLLRRRLRLRLLGRGLGRRRGRLG"))
