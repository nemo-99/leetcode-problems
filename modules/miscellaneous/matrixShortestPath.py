'''
    https://leetcode.com/problems/shortest-path-in-binary-matrix/
'''
import heapq


def getNeighbors(coordX,coordY):
    return [(coordX+1,coordY),
    (coordX,coordY+1),
    (coordX+1,coordY+1),
    (coordX-1,coordY),
    (coordX,coordY-1),
    (coordX-1,coordY-1),
    (coordX+1,coordY-1),
    (coordX-1,coordY+1)]


def shortestPathBinaryMatrix(grid: list[list[int]]) -> int:
    if grid[0][0] == 1:
        return -1

    goalCoord = len(grid) - 1
    fringe = []
    heapq.heappush(fringe,(1,(0,0)))
    visited = set()
    fringeRecords = {}

    while len(fringe) != 0:
        payload = heapq.heappop(fringe)
        gX = payload[0] + 1
        coord = payload[1]
        visited.add(coord)

        if coord == (goalCoord, goalCoord):
            return payload[0]

        neighbors = getNeighbors(*coord)

        for neighbor in neighbors:
            if neighbor in visited:
                continue

            if neighbor[0] < 0 or neighbor[1] < 0 or neighbor[0] == (goalCoord + 1) or neighbor[1] == (goalCoord + 1):
                continue

            if grid[neighbor[0]][neighbor[1]] == 1:
                continue

            if neighbor in fringeRecords:
                if fringeRecords[neighbor] > gX:
                    idx = fringe.index(neighbor)
                    del fringe[idx]
                    heapq.heappush(fringe,(gX, neighbor))
            else:
                heapq.heappush(fringe,(gX, neighbor))
                fringeRecords[neighbor] = gX


    return -1


if __name__ == '__main__':
    grid = [[0,0,0],[1,1,0],[1,1,0]]
    print(shortestPathBinaryMatrix(grid))