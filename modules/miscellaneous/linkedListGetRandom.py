'''
   https://leetcode.com/problems/linked-list-random-node/ 
'''

import random

class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:

    def __init__(self,head) -> None:
        self.head = head
        tmpNode = head

        arr = []


        while tmpNode is not None:
            arr.append(tmpNode.val)
            tmpNode = tmpNode.next
        
        self.arr = arr

    def getRandom(self) -> int:
        choice = random.randint(0,len(self.arr)-1)

        return self.arr[choice]
