'''
    https://leetcode.com/problems/all-elements-in-two-binary-search-trees/
'''


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


def getAllElements(root1: TreeNode, root2: TreeNode) -> list[int]:
    def inOrderTraverse(root):

        ans = []

        if root.left is not None:
            ans.extend(inOrderTraverse(root.left))

        ans.extend(root.val)

        if root.right is not None:
            ans.extend(inOrderTraverse(root.right))

        return ans

    root1Sorted = inOrderTraverse(root1)
    root2Sorted = inOrderTraverse(root2)

    root1Index = 0
    root2Index = 0

    ans = []

    while root1Index < len(root1Sorted) or root2Index < len(root2Sorted):
        if root1Sorted[root1Index] <= root2Sorted[root2Index]:
            ans.append(root1Sorted[root1Index])
            root1Index += 1
        else:
            ans.append(root2Sorted[root2Index])
            root2Index += 1

    while root1Index < len(root1Sorted):
        ans.append(root1Sorted[root1Index])
        root1Index += 1

    while root2Index < len(root2Sorted):
        ans.append(root2Sorted[root2Index])
        root2Index += 1

    return ans
