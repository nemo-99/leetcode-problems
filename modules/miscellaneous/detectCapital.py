'''
https://leetcode.com/problems/detect-capital/submissions/
'''


def detectCapitalUse( word: str) -> bool:
    if word[0].islower():
        for char in word:
            if not char.islower():
                return False

        return True

    else:
        count = 0
        for char in word:
            if char.isupper():
                count += 1

        if count == 1 or count == len(word):
            return True
        else:
            return False

if __name__ == '__main__':
    word = 'g'
    print(detectCapitalUse(word))