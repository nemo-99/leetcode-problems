import java.util.Arrays;

/*
    Given an integer array nums and an integer target,
    find three integers in the array with their sum
    closest to target and return their sum

    Input: nums = [-1,2,1,-4], target = 1
        Output: 2

*/

public class ThreeSumClosest {
    public int threeSumClosest(int[] nums, int target) {
        Arrays.sort(nums);

        int minDiff = Integer.MAX_VALUE;
        int closestSum = -1;

        for (int i = 0; i < nums.length; i++) {
            int lElement = i + 1;
            int rElement = nums.length - 1;

            while (lElement < rElement) {
                int tmpSum = nums[lElement] + nums[rElement] + nums[i];
                if (Math.abs(tmpSum - target) < minDiff) {
                    minDiff = Math.abs(tmpSum - target);
                    closestSum = tmpSum;
                }

                if (tmpSum < target) {
                    lElement++;
                } else if (tmpSum > target) {
                    rElement--;
                } else {
                    break;
                }
            }
        }
        return closestSum;
    }

    /*
     * [1,2,4,8,16,32,64,128] 82
     */
    public static void main(String[] args) {
        ThreeSumClosest threeSumClosest = new ThreeSumClosest();
        int[] nums = { 1, 2, 4, 8, 16, 32, 64, 128 };
        int target = 82;
        System.out.println(threeSumClosest.threeSumClosest(nums, target));
    }
}
