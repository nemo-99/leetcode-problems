"""
    https://leetcode.com/problems/find-the-town-judge/
"""


def findJudge(n: int, trust) -> int:
    trustedBy = {}
    trusts = {}

    for trustees in trust:
        if trustees[1] not in trustedBy:
            trustedBy[trustees[1]] = []
            trustedBy[trustees[1]].append(trustees[0])
        else:
            trustedBy[trustees[1]].append(trustees[0])

        if trustees[0] not in trusts:
            trusts[trustees[0]] = []
            trusts[trustees[0]].append(trustees[1])
        else:
            trusts[trustees[0]].append(trustees[1])

    for citizen in trustedBy.keys():
        if len(trustedBy[citizen]) == n - 1 and citizen not in trusts:
            return citizen

    return -1


if __name__ == "__main__":
    trust = [[1, 3], [2, 3], [3, 1]]
    print(findJudge(3, trust))
