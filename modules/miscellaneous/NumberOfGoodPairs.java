/*
 * Given an array of integers return the number of good pairs
 * A good pair (i,j) is such that nums[i] == nums[j] and 
 * i < j 
 * Input: nums = [1,2,3,1,1,3]
 * Output: 4
 */ 

import java.util.HashMap;

class NumberOfGoodPairs {
    public static void main(String[] args) {
        int[] array = { 1, 2, 3, 1, 1, 3 };
        System.out.println(new Solution().numIdenticalPairs(array));
    }
}

class Solution {
    public int numIdenticalPairs(int[] nums) {

        HashMap<Integer, Integer> hashMap = new HashMap<>();

        int goodPairCount = 0;
        for (int i = 0; i < nums.length; i++) {
            if (hashMap.containsKey(nums[i])) {
                goodPairCount += hashMap.get(nums[i]);
                hashMap.put(nums[i], hashMap.get(nums[i]) + 1);
            } else {
                hashMap.put(nums[i], 1);
            }
        }
        return goodPairCount;
    }
}
