#include <iostream>

/*
 * Given a linked list,
 * group all odd positioned nodes together
 * followed by the even nodes
 * Input: 2->1->3->5->6->4->7->NULL
 * Output: 2->3->6->7->1->5->4->NULL
 */

struct ListNode {
      int val;
      ListNode *next;
      ListNode() : val(0), next(nullptr) {}
      ListNode(int x) : val(x), next(nullptr) {}
      ListNode(int x, ListNode *next) : val(x), next(next) {}
  };

ListNode* oddEvenList(ListNode* head) {
    if(head == NULL || head->next == NULL)
    {
        return head;
    }
    ListNode * optr = head;
    ListNode * eptr = head->next;
    ListNode * buffeptr = head->next;
    while(eptr != NULL && eptr->next != NULL)
    {
        optr->next = eptr->next;
        eptr->next = eptr->next->next;
        optr = optr->next;
        eptr = eptr->next;
    }
    optr->next = buffeptr;
    return head;
        
    }

int main()
{
}
