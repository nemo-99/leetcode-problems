#include <iostream>

/*
 * Remove all elements from linked lists
 * with value = val
 * Input:  1->2->6->3->4->5->6, val = 6
 * Output: 1->2->3->4->5
 */

struct ListNode {
      int val;
      ListNode *next;
      ListNode() : val(0), next(nullptr) {}
      ListNode(int x) : val(x), next(nullptr) {}
      ListNode(int x, ListNode *next) : val(x), next(next) {}
  };

ListNode* removeElements(ListNode* head, int val) {
    if(head == NULL)
    {
        return NULL;
    }
    while(head != NULL && head->val == val)
    {
        head = head->next;
    }

    if(head == NULL)
    {
         return NULL;
    }

    ListNode * buff = head;
    while(buff->next != NULL)
    {
     if(buff->next->val == val)
     {
         buff->next = buff->next->next;
     }
     else
     {
        buff = buff->next;
     }
    }
    return head;

}

int main()
{
}
