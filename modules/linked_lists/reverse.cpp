#include <iostream>

/* 
 * Reverse a linked list
 * Input: 1->2->3->4->5->NULL
 * Output: 5->4->3->2->1->NULL
 */

struct ListNode {
      int val;
      ListNode *next;
      ListNode() : val(0), next(nullptr) {}
      ListNode(int x) : val(x), next(nullptr) {}
      ListNode(int x, ListNode *next) : val(x), next(next) {}
  };

ListNode* reverseList(ListNode* head) {
    if( head == NULL)
    {
        return NULL;
    }
    ListNode * buff = head;

    while(buff->next != NULL)
    {
        ListNode * tmp = new ListNode(buff->next->val);
        tmp->next = head;
        head = tmp;
        buff->next = buff->next->next;
    }

    return head;

        
}

int main()
{
}
