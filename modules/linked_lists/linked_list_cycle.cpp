#include <iostream>
using namespace std;

/*
 * Find if linked list has a cycle
 * if yes return true else return false
 */
 

//Structure of Linked list
struct ListNode {
      int val;
      ListNode *next;
      ListNode(int x) : val(x), next(NULL) {}
  };


int main()
{
    ListNode * head;
    ListNode * slowptr = head;
    ListNode * fastptr = head;

    while(slowptr != NULL && fastptr != NULL)
    {
        slowptr = slowptr->next;
        if(fastptr->next == NULL)
        {
            return 0;
        }
        fastptr = fastptr->next->next;
        if(fastptr == slowptr)
        {
            return 1;
        }
    }
    
    return 0;



}

