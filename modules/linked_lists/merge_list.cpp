#include <iostream>

/*
 * Merge two sorted lists to return a 
 * new sorted list
 * Input: l1 = [1,2,4], l2 = [1,3,4]
 * Output: [1,1,2,3,4,4]
 */

struct ListNode {
      int val;
      ListNode *next;
      ListNode() : val(0), next(nullptr) {}
      ListNode(int x) : val(x), next(nullptr) {}
      ListNode(int x, ListNode *next) : val(x), next(next) {}
  };

ListNode* mergeTwoLists(ListNode* l1, ListNode* l2) {
    if(l1 == NULL && l2 == NULL)
    {
        return l1;
    }
    else if( l1 == NULL)
    {
        return l2;
    }
    else if (l2 == NULL)
    {
        return l1;
    }
    ListNode *  buff1 = l1;
    ListNode * buff2 = l2;
    ListNode * ans = new ListNode(l1->val < l2->val ? l1->val : l2->val);
    if(l1->val < l2->val)
    {
        buff1 = buff1->next;
    }
    else
    {
        buff2 = buff2->next;
    }

    ListNode * buff3 = ans;

    while(buff1 != NULL || buff2 != NULL)
    {
        if(buff1 == NULL)
        {
            buff3->next = new ListNode(buff2->val);
            buff3 = buff3->next;
            buff2 = buff2->next;
        }
        else if(buff2 == NULL)
        {
            buff3->next = new ListNode(buff1->val);
            buff3 = buff3->next;
            buff1 = buff1->next;
        }
        else
        {
            if(buff1->val > buff2->val)
            {
                buff3->next = new ListNode(buff2->val);
                buff3 = buff3->next;
                buff2 = buff2->next;
            }
            else
            {
                buff3->next = new ListNode(buff1->val);
                buff3 = buff3->next;
                buff1 = buff1->next;
            }
        }

     }
    return ans;
}

int main()
{
}
