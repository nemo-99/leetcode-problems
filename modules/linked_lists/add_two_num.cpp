#include <iostream>

/*
 * Add two numbers whose digits are stored in a 
 * linked list in reverse order and return their 
 * sum in a linked list similarly stored
 * Input: l1 = [2,4,3], l2 = [5,6,4]
 * Output: [7,0,8]
 */

struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
    ListNode * buffer1 = l1;
    ListNode * buffer2 = l2;
    int sum = l1->val + l2->val;
    buffer1 = buffer1->next;
    buffer2 = buffer2->next;
    ListNode * ans = new ListNode(sum%10);
    int carry = sum/10;
    ListNode * buffer3 = ans;

    while(carry == 1 || buffer1 != NULL || buffer2 != NULL)
    {
        if(buffer1 == NULL && buffer2 == NULL)
        {
            buffer3->next = new ListNode(carry);
            carry = 0;
        }
        else if(buffer1 == NULL)
        {
            sum = (buffer2->val + carry);
            carry = sum/10;
            buffer3->next = new ListNode(sum%10);
            buffer3 = buffer3->next;
            buffer2 = buffer2->next;
        }
        else if(buffer2 == NULL)
        {
            sum = (buffer1->val + carry);
            carry = sum/10;
            buffer3->next = new ListNode(sum%10);
            buffer3 = buffer3->next;
            buffer1 = buffer1->next;
        }
        else
        {
            sum = buffer1->val + buffer2->val + carry;
            carry = sum/10;
            buffer3->next = new ListNode(sum%10);
            buffer1 = buffer1->next;
            buffer2 = buffer2->next;
            buffer3 = buffer3->next;
        }
    }
    return ans;
}

int main()
{
}
