#include <iostream>

/*
 * Given a singly linked list with an additional
 * random pointer which can point to any other node 
 * of the list, create a deep copy of the list
 */

// Definition for a Node.
class Node {
public:
    int val;
    Node* next;
    Node* random;
    
    Node(int _val) {
        val = _val;
        next = NULL;
        random = NULL;
    }
};

Node* copyRandomList(Node* head) {
    if(head == NULL)
    {
        return head;
    }
    Node * buffer1 = head;
    Node * ans = new Node(head->val);
    buffer1 = buffer1->next;
    Node * aptr = ans;

    while(buffer1 != NULL)
    {
        aptr->next = new Node(buffer1->val);
        aptr = aptr->next;
        buffer1 = buffer1->next;
    }

    aptr = ans;
    Node *bptr = ans;
    Node * buffer2 = head;
    buffer1 = head;

    while(buffer1 != NULL)
    {
        if(buffer1->random == NULL)
        {
            aptr->random = NULL;
        }
        else
        {
            buffer2 = head;
            bptr = ans;
            while(buffer2 != NULL)
            {
                if(buffer1->random == buffer2)
                {
                    aptr->random = bptr;
                    break;
                }
                buffer2 = buffer2->next;
                bptr = bptr->next;
            }
        }
        aptr = aptr->next;
        buffer1 = buffer1->next;
    }

    return ans;
        
}

Node* copyRandomList_fast(Node * head)
{
    if(head == NULL)
    {
        return head;
    }

    Node * buffer = head;
    Node * temp;

    while(buffer != NULL)
    {
        temp = new Node(buffer->val);
        temp->next = buffer->next;
        buffer->next = temp;
        buffer = buffer->next->next;
    }

    buffer = head;
    while(buffer != NULL)
    {
        if(buffer->random != NULL)
        {
            buffer->next->random = buffer->random->next;
        }
        else
        {
            buffer->next->random = NULL;
        }
        buffer = buffer->next->next;
    }

    Node * ans = head->next;
    buffer = head;
    Node * buffer2 = ans;

    while(buffer2->next != NULL)
    {
        buffer->next = buffer->next->next;
        buffer2->next = buffer2->next->next;
        buffer = buffer->next;
        buffer2 = buffer2->next;
    }
    
    buffer->next = NULL;
    return ans;
}

int main()
{
}
