#include <iostream>
#include <vector>
using namespace std;

/*
 * Given head of list, rotate list by k places
 * Input: head = [1,2,3,4,5], k = 2
 * Output: [4,5,1,2,3]
 */

struct ListNode {
      int val;
      ListNode *next;
      ListNode() : val(0), next(nullptr) {}
      ListNode(int x) : val(x), next(nullptr) {}
      ListNode(int x, ListNode *next) : val(x), next(next) {}
  };

ListNode* rotateRight(ListNode* head, int k) {
    if(head == NULL)
    {
        return head;
    }
    ListNode * buffer = head;
    int count = 0;
    vector <int> ll;

    while(buffer != NULL)
    {
        ll.push_back(buffer->val);
        buffer = buffer->next;
        count++;
    }

    k = k%count;

    for(int i = count;i>=count - k + 1;i--)
    {
        ListNode * buffer2 = new ListNode(ll[i-1],head);
        head = buffer2;
    }

    buffer = head;
    for(int i = 1;i < count;i++)
    {
        buffer = buffer->next;
    }

    buffer->next = NULL;
    return head;

}

int main()
{
}
