#include <iostream>
using namespace std;

/*
 * MyLinkedList() is a doubly linked list which provides the
 * following functionalities
 * int get(int index) receive val of node at index
 * void addAtHead(int val) add node with val at head
 * void addAtTail(int val) add node with val at tail
 * void addAtIndex(int index, int val) at node at index
 * void deleteAtIndex(int index) delete node at index
 * void display() display all nodes in linked list
 */


class MyLinkedList 
{
    
    int val;
    MyLinkedList * next;
    MyLinkedList * prev;
    int ctr;
    public:
    MyLinkedList()
    {
        ctr = 0;
    }

    int get(int index)
    {
        if(index > ctr-1)
        {
            return -1;
        }
        MyLinkedList * buffer = this;
        int ans = -1;
        int count = 0;
        while(buffer != NULL)
        {
            if(count == index)
            {
                ans = buffer->val;
                break;
            }
            buffer = buffer->next;
            count++;
        }
        return ans;
    }

    void addAtHead(int val)
    {
        MyLinkedList * buffer = new MyLinkedList();
        if(this->ctr == 0)
        {
            //cout << "in here" << endl;
            this->val = val;
            this->next = NULL;
            this->prev = NULL;
        }
        else
        {
            buffer->val = this->val;
            buffer->next = this->next;
            this->val = val;
            this->next = buffer;
            buffer->prev = this;
            this->prev = NULL;
        }
        ctr++;

    }

    void addAtTail(int val)
    {
        if(ctr == 0)
        {
            addAtHead(val);
            return;
        }

        MyLinkedList * buffer = this;
        while(buffer->next != NULL)
        {
            buffer = buffer->next;
        }
        buffer->next = new MyLinkedList();
        buffer->next->val = val;
        buffer->next->next = NULL;
        buffer->next->prev = buffer;
        ctr++;

    }

    void addAtIndex(int index,int val)
    {
        if(index == 0)
        {
            addAtHead(val);
            return;
        }
        if(index > ctr)
        {
            return;
        }
        int count = 0;
        MyLinkedList * buffer = this;
        while(count != index - 1)
        {
            buffer = buffer->next;
            count++;
        }
        MyLinkedList * newnode = new MyLinkedList();
        newnode->val = val;
        newnode->next = buffer->next;
        newnode->prev = buffer;
        if(buffer->next != NULL)
        {
            buffer->next->prev = newnode;
        }
        buffer->next = newnode;
        ctr++;
    }

    void deleteAtIndex(int index)
    {
        if(index >= ctr)
        {
            return;
        }

        int count = 0;
        MyLinkedList * buffer = this;
        if(index == 0)
        {
            if(this->ctr == 1)
            {
                //delete this;
                this->ctr--;
                return;
            }
            else
            {
                MyLinkedList *node = this;
                this->val = this->next->val;
                this->next = this->next->next;
                this->next->prev = this;
                this->prev = NULL;
                this->ctr--;
                return;
            }


        }
        while(count != index -1)
        {
            count++;
            buffer = buffer->next;

        }
        MyLinkedList *tmpnode = buffer->next->next;
        buffer->next = tmpnode; 
        if(tmpnode != NULL)
        {
            tmpnode->prev = buffer;
        }
        ctr--;
        
    }

    void display()
    {
        if(this->ctr == 0)
        {
            cout << " " << endl;
            return;
        }


        MyLinkedList *buffer =  this;
        while(buffer != NULL)
        {
            cout << buffer->val << "->";
            buffer = buffer->next;
        }
        cout << endl;
    }


};

int main()
{
    MyLinkedList myLinkedList = MyLinkedList();
    
    myLinkedList.addAtHead(1);
    myLinkedList.deleteAtIndex(0);    


}
