#include <iostream>
#include <math.h>

/*
 * Given two linked lists,
 * return node where lists interect
 * else return null
 *
 */


struct ListNode {
      int val;
      ListNode *next;
      ListNode(int x) : val(x), next(NULL) {}
  };

ListNode *getIntersectionNode(ListNode *headA, ListNode *headB) {

    ListNode * buffa = headA;
    ListNode * buffb = headB;
    int acount = 0,bcount = 0;
    while(buffa != NULL || buffb != NULL)
    {
        if(buffa != NULL)
        {
            buffa = buffa->next;
            acount++;
        }
        if(buffb != NULL)
        {
            buffb = buffb->next;
            bcount++;
        }

    }

    ListNode * smalllist = acount > bcount ? headB : headA;
    ListNode * biglist = acount > bcount ? headA : headB;
    int diff = abs(acount - bcount);

    while(diff > 0)
    {
        biglist = biglist->next;
        diff--;
    }

    while(biglist != smalllist)
    {
        if(biglist == NULL || smalllist == NULL)
        {
            return NULL;
        }
        smalllist = smalllist->next;
        biglist = biglist->next;
    }
    return smalllist;
}

int main()
{
    ListNode * a = new ListNode(5);
    ListNode * b = new ListNode(7);
    getIntersectionNode(a,b);
}
