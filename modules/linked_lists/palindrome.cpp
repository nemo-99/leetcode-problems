#include <iostream>

/*
 * Determine if a given linked list is a palindrome
 * Input: 1->2->2->1
 * Output: true
 */

struct ListNode {
      int val;
      ListNode *next;
      ListNode() : val(0), next(nullptr) {}
      ListNode(int x) : val(x), next(nullptr) {}
      ListNode(int x, ListNode *next) : val(x), next(next) {}
  };

bool isPalindrome(ListNode* head) {
    if(head == NULL || head->next == NULL)
    {
        return true;
    }

    int count = 0;

    ListNode * buffer = head;

    while(buffer != NULL)
    {
        buffer = buffer->next;
        count++;
    }

    buffer = head;
    count = count%2 == 0 ? count/2 - 1 : count/2;
    for(int i = 0;i < count;i++)
    {
        buffer = buffer->next;
    }

    ListNode * abuffer = buffer;
    buffer = buffer->next;

    while(buffer->next != NULL)
    {
        ListNode * tmp = new ListNode(buffer->next->val,abuffer->next);
        abuffer->next = tmp;
        buffer->next = buffer->next->next;
    }

    buffer = head;
    abuffer = abuffer->next;

    while(abuffer != NULL)
    {
        if(abuffer->val != buffer->val)
            return false;
        else
        {
            abuffer = abuffer->next;
            buffer = buffer->next;
        }
    }

    return true;
    }

int main()
{
}
