#include <iostream>
using namespace std;

/*
 * Given a linked list
 * return null if no cycle exists
 * else return node where cycle begins
 * for details about solution refer to link
 * https://stackoverflow.com/questions/2936213/explain-how-finding-cycle-start-node-in-cycle-linked-list-work
 */


struct ListNode {
      int val;
      ListNode *next;
      ListNode(int x) : val(x), next(NULL) {}
  };

ListNode* detectCycle(ListNode * head)
{
    ListNode * slowptr = head;
    ListNode * fastptr = head;
    bool cycledetect = false;

    while(fastptr != NULL && fastptr->next != NULL)
    {
        slowptr = slowptr->next;
        fastptr = fastptr->next->next;

        if(slowptr == fastptr)
        {
            cycledetect = true;
            break;
        }
    }

    if(cycledetect == false)
    {
        return NULL;
    }

    fastptr = head;
    while(fastptr != slowptr)
    {
        fastptr = fastptr->next;
        slowptr = slowptr->next;
    }
    return slowptr;
}

int main()
{
    ListNode * head; 
    detectCycle(head);

}
