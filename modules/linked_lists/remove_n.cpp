#include <iostream>

/*
 * Given linked list head and integer n,
 * remove nth node of linked list from the end
 * Input: head = [1,2,3,4,5], n = 2
 * Output: [1,2,3,5]
 */

struct ListNode {
      int val;
      ListNode *next;
      ListNode() : val(0), next(nullptr) {}
      ListNode(int x) : val(x), next(nullptr) {}
      ListNode(int x, ListNode *next) : val(x), next(next) {}
  };

 ListNode* removeNthFromEnd(ListNode* head, int n) {
     if(head->next == NULL)
     {
         return NULL;
     }

     ListNode *frontptr = head;
     ListNode *backptr = head;
     int tmp = n;

     while(n > 0)
     {
         frontptr = frontptr->next;
         n--;
     }

     if(frontptr == NULL)
     {
         head = head->next;
         return head;
     }

     while(frontptr->next != NULL)
     {
         frontptr = frontptr->next;
         backptr = backptr->next;
     }

     backptr->next = backptr->next->next;

     
     return head;   
    }

int main()
{
}
