#include <iostream>
#include <vector>
using namespace std;

/*
 * Given a doubly level linked list which in 
 * addition also has optional child nodes,
 * flatten the linked list.
 * Input: head = [1,2,3,4,5,6,null,null,null,7,8,9,10,null,null,11,12]
 * Output: [1,2,3,7,8,11,12,9,10,4,5,6]
 */

class Node {
public:
    int val;
    Node* prev;
    Node* next;
    Node* child;
};

void depth_search(Node *& buff1, Node *& buff2)
{
    while(buff1 != NULL)
    {
        buff2->next = new Node();
        buff2->next->val = buff1->val;
        buff2->next->prev = buff2;
        buff2->next->child = NULL;
        buff2 = buff2->next;
        if(buff1->child != NULL)
        {
            depth_search(buff1->child,buff2);
        }
        buff1 = buff1->next;
    }

}

Node* flatten(Node* head) {
    if(head == NULL)
    {
        return head;
    }
    Node * buffer1 = head;
    Node * ans = new Node();
    ans->val = buffer1->val;
    ans->prev = NULL;
    ans->child = NULL;
    Node * buffer2 = ans;

    if(buffer1->child != NULL)
    {
        depth_search(buffer1->child,buffer2);
    }
    buffer1 = buffer1->next;
    depth_search(buffer1,buffer2);
    buffer2->next = NULL;
    return ans;
}

int main()
{
}
