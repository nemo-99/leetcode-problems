/*
 * Design Circular Queue
 */

class MyCircularQueue {
public:
    int * arr;
    int head;
    int tail;
    int count;
    int size;
    MyCircularQueue(int k)
    {
        arr = new int[k];
        head = 0;
        count = 0;
        tail = 0;
        arr[0] = -1;
        size = k;
    }

    bool enQueue(int value)
    {
        if(count == size)
        {
            return false;
        }
        else if(count == 0)
        {
            arr[head] = value;
            count++;
            return true;
        }

        count++;
        tail = (tail+1)%size;
        arr[tail] = value;
        return true;
    }

    bool deQueue()
    {
        if(count == 0)
        {
            return false;
        }
        else if(count == 1)
        {
            arr[head] = -1;
            count--;
            return true;
        }

        count--;
        arr[head] = -1;
        head = (head+1)%size;
        return true;
    }

    int Front()
    {
        return arr[head];
    }

    int Rear()
    {
        return arr[tail];
    }

    bool isEmpty()
    {
        return count == 0 ? true : false;
    }

    bool isFull()
    {
       return count == size ? true : false;
    }
};

int main()
{
}
