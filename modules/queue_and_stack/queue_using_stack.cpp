#include <stack>
using namespace std;

/*
 * Design a Queue using a stack
 */

class MyQueue
{
public:
    /** Initialize your data structure here. */
    stack <int> q;
    MyQueue()
    {

    }

    /** Push element x to the back of queue. */
    void push(int x)
    {
        if(q.empty())
        {
            q.push(x);
        }
        else
        {
            stack <int> aux_stk;
            while(!q.empty())
            {
                aux_stk.push(q.top());
                q.pop();
            }
            q.push(x);
            while(!aux_stk.empty())
            {
                q.push(aux_stk.top());
                aux_stk.pop();
            }
        }
    }

    /** Removes the element from in front of queue and returns that element. */
    int pop()
    {
        int ans = q.top();
        q.pop();
        return ans;
    }

    /** Get the front element. */
    int peek()
    {
        return q.top();
    }

    /** Returns whether the queue is empty. */
    bool empty()
    {
        return q.empty();
    }
};

int main()
{
}
