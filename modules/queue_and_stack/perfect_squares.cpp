#include <bits/stdc++.h>
#include <queue>
#include <unordered_set>
using namespace std;

/*
 * Return the minimum number of
 * perfect squares whose sum will
 * be equal to n
 * Input: n = 12
 * Output: 3
 */

int numSquares(int n)
{
    int sum = 0;
    queue<pair<int,int>> q;
    unordered_set <int> hs;
    int count = 0;
    do
    {
        if(!q.empty())
        {
            sum = q.front().first;
            count = q.front().second;
            q.pop();
        }
        for(int i = 1;(i*i) <= (n-sum);i++)
        {
            if(i*i == n - sum)
            {
                return count+1;
            }
            if(hs.count(sum+i*i) == 0)
            {
                q.push(make_pair(sum+i*i,count+1));
                hs.insert(sum + i*i);
            }
        }

    }while(!q.empty());

    return count;
}

int main()
{
    cout << numSquares(2) << endl;
}
