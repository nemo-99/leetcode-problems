#include <string>
#include <ctype.h>
#include <stack>
#include <iostream>
using namespace std;

/*
 * Given an encoded string in the format
 * k[str] return the decoded string which will
 * be str repeated k times
 * Input: s = "3[a]2[bc]"
 * Output: "aaabcbc"
 */

void recursive_parse(int & i,string &s,string & ans,stack <int> & stk)
{

    int target;
    if(stk.empty())
    {
        target = 1;
    }
    else
    {
        target = stk.top();
    }
    int oldi = i;
    for(int j = 0;j < target;j++)
    {
        i = oldi;
        while(s[i] != ']' && i < s.size())
        {
            if(isdigit(s[i]))
            {
                string num;
                while(s[i] != '[')
                {
                    num += s[i];
                    i++;
                }
                stk.push(stoi(num));
                i += 1;
                recursive_parse(i,s,ans,stk);
            }
            else
            {
                ans += s[i];
            }
            i++;
        }
    }
    if(!stk.empty())
    {
        stk.pop();
    }
}

string decodeString(string s)
{
    string ans = "";
    int i = 0;
    stack <int> stk;
    recursive_parse(i,s,ans,stk);
    return ans;
}

int main()
{
    cout << decodeString("3[a2[c]]") << endl;
}


