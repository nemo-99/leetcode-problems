#include <vector>
#include <iostream>
using namespace std;

/*
 * Given an array of integers and two operators
 * '+' and '-' to be assigned to each number, return
 * number of ways their sum can be equal to the target
 * integer S
 */

int count = 0;

void dfs_compute(int sum, const vector<int> & nums,int i,int S)
{
    if(i == nums.size())
    {
        if(sum == S)
        {
            count++;
        }
    }
    else
    {
        dfs_compute(sum+nums[i],nums,i+1,S);
        dfs_compute(sum-nums[i],nums,i+1,S);
    }
}

int findTargetSumWays(vector<int>& nums, int S)
{
    dfs_compute(0,nums,0,S);
    return count;
}

int main()
{
    vector <int> nums = {42,24,30,14,38,27,12,29,43,42,5,18,0,1,12,44,45,50,21,47};
    cout << findTargetSumWays(nums,38) << endl;
}
