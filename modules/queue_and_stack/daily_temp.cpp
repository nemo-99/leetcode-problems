#include <vector>
#include <stack>
using namespace std;

/*
 * Given a list of daily temperatures, for
 * each day return the number of days until a
 * warmer day. Return 0 if no warmer days are
 * ahead
 */

vector<int> dailyTemperatures(vector<int>& T)
{
    vector <int> ans(T.size(),0);
    stack <int> stk;

    for(int i = 0;i < T.size();i++)
    {
        while( !stk.empty() && T[i] > T[stk.top()])
        {
            ans[stk.top()] = i - stk.top();
            stk.pop();
        }
        stk.push(i);
    }

    return ans;
}

int main()
{
}
