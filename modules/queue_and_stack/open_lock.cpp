#include <unordered_set>
#include <unordered_map>
#include <bits/stdc++.h>
#include <vector>
using namespace std;

/*
 * Given a list of combinations(deadlocks) and a target
 * combination, calculate the minimum steps needed
 * to reach that combination in a circular lock while
 * avoiding the list of deadlocks
 *  Input: deadends = ["0201","0101","0102","1212","2002"], target = "0202"
 *  Output: 6
 */

int openLock(vector<string>& deadends, string target)
{
    if(target == "0000")
    {
        return 0;
    }

    unordered_set <string> de;
    for(auto i : deadends)
    {
        de.insert(i);
    }

    unordered_map <char,char> up = {{'0','1'},{'1','2'}, {'2','3'},{'3','4'},{'4','5'},{'5','6'},{'6','7'},{'7','8'},{'8','9'},{'9','0'}};
    unordered_map <char,char> down = {{'0','9'},{'1','0'},{'2','1'},{'3','2'},{'4','3'},{'5','4'},{'6','5'},{'7','6'},{'8','7'},{'9','8'}};

    queue <pair<string,int>> q;
    q.push(make_pair("0000",0));
    unordered_set <string> memo;
    memo.insert("0000");

    while(!q.empty())
    {

        if(de.count(q.front().first) != 0)
        {
            q.pop();
            continue;
        }

        string fstring = q.front().first;
        int count = q.front().second;

        for(int i = 0;i < target.size();i++)
        {
            string tmp = fstring;
            char c = tmp[i];
            tmp[i] = up[c];

            if(tmp == target)
            {
                return count+1;
            }
            if(memo.count(tmp) == 0)
            {
                q.push(make_pair(tmp,count + 1));
                memo.insert(tmp);
            }

            tmp = fstring;
            c = tmp[i];
            tmp[i] = down[c];

            if(tmp == target)
            {
                return count+1;
            }

            if(memo.count(tmp) == 0)
            {
                q.push(make_pair(tmp,count + 1));
                memo.insert(tmp);
            }
        }
        q.pop();
    }

    return -1;

}

int main()
{
}
