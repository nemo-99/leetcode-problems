#include <stack>
#include <iostream>
#include <unordered_map>

/*
 * Given a string just containing parentheses
 * determine if their order is valid
 */

bool isValid(std::string s)
{
    std::unordered_map <char,char> hm = {{'{','}'},{'[',']'},{'(',')'}};
    std::stack <char> stk;

    for(char i : s)
    {
        if(hm.count(i) != 0)
        {
            stk.push(i);
        }
        else if(!stk.empty() && hm[stk.top()] == i)
        {
            stk.pop();
        }
        else
        {
            return false;
        }
    }

    if(stk.empty())
    {
        return true;
    }
    else
    {
        return false;
    }

}

int main()
{
    std::cout << isValid("]") << std::endl;
}
