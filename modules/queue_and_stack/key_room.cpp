#include <vector>
using namespace std;

/*
 * Given a vector of rooms, with each room containing keys to
 * other rooms, starting in room 0, determine if you can visit
 * all the other rooms
 * Input: [[1,3],[3,0,1],[2],[0]]
 * Output: false
 */

void dfs_search(const vector<vector<int>> & rooms,int room,vector<int>&visited,int & count)
{
    visited[room] = 1;
    count++;

    for(int key : rooms[room])
    {
        if(visited[key] == 0)
        {
            dfs_search(rooms,key,visited,count);
        }
    }
}

bool canVisitAllRooms(vector<vector<int>>& rooms)
{
    vector <int> visited(rooms.size(),0);
    int count = 0;
    dfs_search(rooms,0,visited,count);
    if(count == rooms.size())
    {
        return true;
    }
    return false;
}

int main()
{
}
