#include <vector>
using namespace std;

/*
 * Implement floodfill algorithm. Given a
 * 2D matrix image, fill all 4-directional neighboring
 * pixels having the same value with newColor
 * Input:
 * image = [[1,1,1],[1,1,0],[1,0,1]]
 * sr = 1, sc = 1, newColor = 2
 * Output: [[2,2,2],[2,2,0],[2,0,1]]
 */

int length;
int width;

bool isvalid(int i,int j)
{
    if(i >= 0 && i < length && j >= 0 && j < width)
    {
        return true;
    }
    return false;
}

void dfsfill(int i,int j,vector <vector<int>> & image,const int & newColor,vector <vector <int>> & visited,int oldcolor)
{
    vector <int> vals = {1,-1};

    for(int val : vals)
    {
        if(isvalid(i+val,j) && visited[i+val][j] == 0 && image[i+val][j] == oldcolor)
        {
            visited[i+val][j] = 1;
            image[i+val][j] = newColor;
            dfsfill(i+val,j,image,newColor,visited,oldcolor);
        }

        if(isvalid(i,j+val) && visited[i][j+val] == 0 && image[i][j+val] == oldcolor)
        {
            visited[i][j+val] = 1;
            image[i][j+val] = newColor;
            dfsfill(i,j+val,image,newColor,visited,oldcolor);
        }
    }
}

vector<vector<int>> floodFill(vector<vector<int>>& image, int sr, int sc, int newColor)
{
    length = image.size();
    width = image[0].size();
    vector <vector <int>> visited(length,vector<int> (width,0));
    int oldcolor = image[sr][sc];
    image[sr][sc] = newColor;
    visited[sr][sc] = 1;
    dfsfill(sr,sc,image,newColor,visited,oldcolor);
    return image;
}

int main()
{
}
