#include <vector>
#include <unordered_set>
using namespace std;

/*
 * For description refer to islands.cpp
 * Problem solved using depth first search
 */

bool is_valid_idx(int i,int j,int length,int width,vector <vector<char>> & grid)
{
    if(i < length && i >= 0 && j < width && j >= 0 && grid[i][j] == '1')
    {
        return true;
    }

    return false;
}

string serialise(int i,int j)
{
    return to_string(i) + "," + to_string(j);
}

void dfs(int i,int j,unordered_set <string> & hs,vector <vector <char>> & grid)
{
    vector <int> values = {1,-1};
    hs.insert(serialise(i,j));
    for(int val : values)
    {
        if(is_valid_idx(i+val,j,grid.size(),grid[0].size(),grid) && hs.count(serialise(i+val,j)) == 0)
        {
            dfs(i+val,j,hs,grid);
        }
        if(is_valid_idx(i,j+val,grid.size(),grid[0].size(),grid) && hs.count(serialise(i,j+val)) == 0)
        {
            dfs(i,j+val,hs,grid);
        }
    }
}

int numIslands(vector<vector<char>>& grid)
{
    unordered_set <string> hs;
    int islands = 0;
    for(int i = 0;i < grid.size();i++)
    {
        for(int j = 0;j < grid[i].size();j++)
        {
            if(hs.count(serialise(i,j)) == 0 && grid[i][j] == '1')
            {
                islands++;
                dfs(i,j,hs,grid);
            }
        }
    }

    return islands;
}

int main()
{
}
