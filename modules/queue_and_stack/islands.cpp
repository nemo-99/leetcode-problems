#include <iostream>
#include <vector>
#include <unordered_set>
#include <queue>
using namespace std;

/*
 * Given a 2D matrix consisting of 1s(land) and 0s(water)
 * return the total number of islands in the matrix. Assume
 * the matrix is surrounded by water
 */

void bfs_search(vector<vector<char>>& grid,int i,int j, unordered_set <string> & hs)
{
    queue <vector<int>> q;
    q.push({i,j});
    hs.insert(to_string(i) + ","+ to_string(j));

    int width = grid[0].size();
    int height = grid.size();

    while(!q.empty())
    {

        if(j+1 < width && hs.count(to_string(i)+ "," + to_string(j+1)) == 0 && grid[i][j+1] == '1')
        {
            q.push({i,j+1});
            hs.insert(to_string(i)+ "," + to_string(j+1));
        }

        if(j-1 >= 0 && hs.count(to_string(i)+ "," + to_string(j-1)) == 0 && grid[i][j-1] == '1')
        {
            q.push({i,j-1});
            hs.insert(to_string(i)+ "," + to_string(j-1));
        }

        if(i-1 >= 0 && hs.count(to_string(i-1)+ "," + to_string(j)) == 0 && grid[i-1][j] == '1')
        {
            q.push({i-1,j});
            hs.insert(to_string(i-1)+ "," + to_string(j));
        }

        if(i+1 < height && hs.count(to_string(i+1)+ "," + to_string(j)) == 0 && grid[i+1][j] == '1')
        {
            q.push({i+1,j});
            hs.insert(to_string(i+1)+ "," + to_string(j));
        }

        q.pop();
    }

}

int numIslands(vector<vector<char>>& grid)
{
    unordered_set <string> hs;
    int islands = 0;
    for(int i = 0;i < grid.size();i++)
    {
        for(int j = 0;j < grid[i].size();j++)
        {
            if(grid[i][j] == '0' || hs.count(to_string(i)+ "," + to_string(j)) != 0)
            {
                continue;
            }
            else
            {
                islands++;
                bfs_search(grid,i,j,hs);
            }
        }
    }
    return islands;
}

int main()
{
}
