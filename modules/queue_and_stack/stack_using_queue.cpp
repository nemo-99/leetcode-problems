#include <queue>
using namespace std;

/*
 * Design a Stack using a Queue
 */

class MyStack
{
public:
    /** Initialize your data structure here. */
    queue <int> stk;
    MyStack()
    {

    }

    /** Push element x onto stack. */
    void push(int x)
    {
        if(stk.empty())
        {
            stk.push(x);
        }
        else
        {
            queue <int> newq;
            newq.push(x);
            while(!stk.empty())
            {
                newq.push(stk.front());
                stk.pop();
            }
            stk = newq;
        }
    }

    /** Removes the element on top of the stack and returns that element. */
    int pop()
    {
        int ans = stk.front();
        stk.pop();
        return ans;
    }

    /** Get the top element. */
    int top()
    {
        return stk.front();
    }

    /** Returns whether the stack is empty. */
    bool empty()
    {
        return stk.empty();
    }
};

int main()
{
}
