#include <vector>
#include <iostream>
#include <stack>
#include <string>
using namespace std;

/*
 * Given a mathematical expression in
 * postfix (reverse polish) notation
 * evaluate the expression
 * Input: ["2", "1", "+", "3", "*"]
 * Output: 9
 */

int evalRPN(vector<string>& tokens)
{
    stack <int> stk;

    for(string token : tokens)
    {
        if(token != "+" && token != "-" && token != "/" && token != "*")
        {
            stk.push(stoi(token));
        }
        else
        {
            int a = stk.top();
            stk.pop();
            int b = stk.top();
            stk.pop();
            switch(token[0])
            {
                case '+':
                    stk.push(a+b);
                    break;
                case '-':
                    stk.push(b-a);
                    break;
                case '*':
                    stk.push(b*a);
                    break;
                case '/':
                    stk.push(b/a);
                    break;
            }
        }
    }

    return stk.top();
}

int main()
{
    vector <string> q = {"10", "6", "9", "3", "+", "-11", "*", "/", "*", "17", "+", "5", "+"};
    cout << evalRPN(q) << endl;
}
