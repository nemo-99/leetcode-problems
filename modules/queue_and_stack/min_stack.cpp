#include <vector>
using namespace std;

/*
 * Design a stack with
 * push,pop,top and fetch
 * minimum element functionalities
 * in constant time
 */

class MinStack
{
public:
    /** initialize your data structure here. */
    vector <int> stack;
    vector <int> min_idx;

    MinStack()
    {

    }

    void push(int x)
    {
        if(stack.empty())
        {
            min_idx.push_back(0);
        }
        else if(x < stack[min_idx.back()])
        {
           min_idx.push_back(stack.size());
        }
        stack.push_back(x);
    }

    void pop()
    {
        stack.pop_back();
        if(min_idx.back() == stack.size())
        {
            min_idx.pop_back();
        }
    }

    int top()
    {
        return stack.back();
    }

    int getMin()
    {
        return stack[min_idx.back()];
    }
};

int main()
{
}
