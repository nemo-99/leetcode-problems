#include <vector>
using namespace std;

/*
 * Given a matrix of 1s and 0s,for each number
 * return its distance from the nearest 0
 * Input:
 *[[0,0,0],
 *[0,1,0],
 *[1,1,1]]

 * Output:
 *[[0,0,0],
 *[0,1,0],
 *[1,2,1]]
 */

int length;
int width;

bool is_valid_idx(int i,int j)
{
    if(i >= 0 && i < length && j >= 0 && j < width)
    {
        return true;
    }
    return false;
}

int dfs_search(const vector<vector<int>> & matrix,int i,int j,vector <vector<int>> &ans)
{
    ans[i][j] = -2;
    vector <int> vals = {+1,-1};
    int int_ans = length+width+1;
    for(int val : vals)
    {
        if(is_valid_idx(i+val,j))
        {
            if(matrix[i+val][j] == 0)
            {
                ans[i][j] = 1;
                return 1;
            }
            else if(ans[i+val][j] > 0)
            {
                int tmp = ans[i+val][j] + 1;
                if(tmp < int_ans)
                {
                    int_ans = tmp;
                }
            }
            else if(ans[i+val][j] != -2)
            {
                int tmp = dfs_search(matrix,i+val,j,ans) + 1;
                if(tmp < int_ans)
                {
                    int_ans = tmp;
                }
            }
        }


        if(is_valid_idx(i,j+val))
        {
            if(matrix[i][j+val] == 0)
            {
                ans[i][j] = 1;
                return 1;
            }
            else if(ans[i][j+val] > 0)
            {
                int tmp = ans[i][j+val] + 1;
                if(tmp < int_ans)
                {
                    int_ans = tmp;
                }
            }
            else if(ans[i][j+val] != -2)
            {
                int tmp = dfs_search(matrix,i,j+val,ans) + 1;
                if(tmp < int_ans)
                {
                    int_ans = tmp;
                }
            }
        }
    }
    ans[i][j] = int_ans;
    return ans[i][j];
}

vector<vector<int>> updateMatrix(vector<vector<int>>& matrix)
{
    length = matrix.size();
    width = matrix[0].size();
    vector<vector <int>> ans(matrix.size(),vector<int>(matrix[0].size(),-1));

    for(int i = 0;i < matrix.size();i++)
    {
        for(int j = 0;j < matrix[i].size();j++)
        {
            if(matrix[i][j] == 0)
            {
                ans[i][j] = 0;
            }
            else if(matrix[i][j] == 1)
            {
                dfs_search(matrix,i,j,ans);
            }
        }
    }
    return ans;
}

int main()
{
}
