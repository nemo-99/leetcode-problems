#include <vector>
#include <unordered_map>
using namespace std;

/*
 * Given a graph, return a
 * deep copy of the graph
 */


class Node {
public:
    int val;
    vector<Node*> neighbors;
    Node() {
        val = 0;
        neighbors = vector<Node*>();
    }
    Node(int _val) {
        val = _val;
        neighbors = vector<Node*>();
    }
    Node(int _val, vector<Node*> _neighbors) {
        val = _val;
        neighbors = _neighbors;
    }
};

void dfscopy(Node * original, Node * & copy,unordered_map <int,Node*> & hm)
{
    hm[copy->val] = copy;

    for(Node * neighbor : original->neighbors)
    {
        if(hm.count(neighbor->val) != 0)
        {
            copy->neighbors.push_back(hm[neighbor->val]);
        }
        else
        {
            copy->neighbors.push_back(new Node(neighbor->val));
            dfscopy(neighbor,copy->neighbors.back(),hm);
        }
    }
}

Node* cloneGraph(Node* node)
{
    if(node == NULL)
    {
        return node;
    }

    unordered_map <int,Node *> hm;
    Node * copy = new Node(node->val);

    dfscopy(node,copy,hm);
    return copy;
}

int main()
{
}
