#include <unordered_set>
using namespace std;

/*
 * Return if a given number is a Happy
 * Number.
 * A number is happy if iteratively summing
 * the square of its digits even leads to the
 * sum being 1. otherwise the sum will loop
 * endlessly in a cycle
 */

int calc_sum(int n)
{
    int sum = 0;
    while(n > 0)
    {
        sum += (n%10)*(n%10);
        n = n/10;
    }
    return sum;
}

bool isHappy(int n) 
{
    unordered_set <int> hs;
    int sum = calc_sum(n);
    while(sum != 1)
    {
        if(hs.count(sum) == 1)
        {
            return false;
        }
        hs.insert(sum);
        sum = calc_sum(sum);
    }

    return true;
}
