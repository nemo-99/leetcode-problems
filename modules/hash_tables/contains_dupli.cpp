#include <unordered_map>
#include <vector>
using namespace std;

/*
 * Find if an array of integers contains duplicates
 * and the absolute distance between those numbers
 * is atmost k
 */

bool containsNearbyDuplicate(vector<int>& nums, int k) 
{
    unordered_map <int,int> hm;
    for(int i = 0;i < nums.size();i++)
    {
       if(hm.count(nums[i]) != 0 && (i - hm[nums[i]]) <= k)
       {
           return true;
       }
       hm[nums[i]] = i;
    }

    return false;
}

int main()
{
}
