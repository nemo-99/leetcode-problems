#include <unordered_map>
#include <stdlib.h>
#include <vector>
using namespace std;

/*
 * Implement a Randomized set
 * with insert,remove and
 * getRandom functionalities
 */

class RandomizedSet {
public:
    /** Initialize your data structure here. */
    unordered_map <int,int> hs;
    vector <int> arr;
    RandomizedSet() 
    {
        
    }
    
    /** Inserts a value to the set. Returns true if the set did not already contain the specified element. */
    bool insert(int val) 
    {
        if(hs.count(val) == 0)
        {
            arr.push_back(val);
            hs[val] = arr.size()-1;
            return true;
        }
        return false;
    }
    
    /** Removes a value from the set. Returns true if the set contained the specified element. */
    bool remove(int val) 
    {
        if(hs.count(val) != 0)
        {
            // swap val and arr.back()
            // pop arr.back()
            int tmp = arr.back();
            arr.back() = val;
            arr[hs[val]] = tmp;
            hs[tmp] = hs[val]; 
            hs.erase(val);
            arr.pop_back();
            return true;
        }
        return false;
    }
    
    /** Get a random element from the set. */
    int getRandom() 
    {
        int random = rand() % arr.size();
        return arr[random];  
    }
};

int main()
{
}
