#include <unordered_set>
using namespace std;

/*
 * Return length of longest non-repeating
 * substring in a given string s
 */

int lengthOfLongestSubstring(string s) 
{
    int maxcount = 0;
    unordered_set <char> hs;

    int i = 0;
    int j = 0;
    while(i < s.size())
    {
        if(hs.count(s[i]) != 0)
        {
            hs.erase(s[j]);
            j++;
        }
        else
        {
            hs.insert(s[i]);
            i++;
        }

        if(maxcount < i-j)
        {
            maxcount = i-j;
        }
    }

    return maxcount;
}

int main()
{
}
