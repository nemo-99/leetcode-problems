#include <unordered_map>
using namespace std;

/*
 * Check if two strings are isomorphic
 * Two strings are isomorphic if
 * characters in one string can be 
 * replaced to get the other string
 */

bool isIsomorphic(string s, string t) 
{
    unordered_map <char,char> hm1,hm2;
    string s1 = "";
    string t1 = "";

    for(int i = 0;i < s.size();i++)
    {
        if(hm1.count(s[i]) == 0)
        {
            hm1[s[i]] = t[i];
        }

        if(hm2.count(t[i]) == 0)
        {
            hm2[t[i]] = s[i];
        }
        s1 += hm2[t[i]];
        t1 += hm1[s[i]];
    }

    if(s1 == s && t1 == t)
    {
        return true;
    }
    else
    {
        return false;
    }
}

int main()
{
}
