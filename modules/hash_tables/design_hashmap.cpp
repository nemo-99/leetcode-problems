#include <vector>
class MyHashMap {

/*
 * Construct a hashmap in the given 
 * range of [0,1000001]
 */
    
public:
    std::vector <int> hashmap;
    MyHashMap() {
        hashmap = std::vector<int>(1000001,-1);
    }
    
    void put(int key, int value) {
        hashmap[key] = value;
    }
    
    int get(int key) {
        return hashmap[key];
    }
    
    void remove(int key) {
        hashmap[key] = -1;    
    }
};

int main()
{
}
