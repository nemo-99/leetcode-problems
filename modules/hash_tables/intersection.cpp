#include <vector>
#include <unordered_set>
using namespace std;

/*
 * Given two arrays, return their intersection.
 * The intersection must not contain any duplicates
 */

vector<int> intersection(vector<int>& nums1, vector<int>& nums2) 
{
    unordered_set <int> hs;
    vector <int> ans;

    for(auto i = nums1.begin();i != nums1.end();i++)
    {
        hs.insert(*i);
    }

    for(auto i = nums2.begin();i != nums2.end();i++)
    {
        if(hs.count(*i) == 1)
        {
            ans.push_back(*i);
            hs.erase(*i);
        }
    }
    return ans;
        
}

int main()
{
}
