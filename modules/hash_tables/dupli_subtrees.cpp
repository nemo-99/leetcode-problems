#include <vector>
#include <unordered_map>
#include <string>
using namespace std;

/*
 * Given a binary tree, search for duplicate subtrees,
 * and if found,
 * Return root of any one of the duplicate subtrees
 */

struct TreeNode {
      int val;
      TreeNode *left;
      TreeNode *right;
      TreeNode() : val(0), left(nullptr), right(nullptr) {}
      TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
      TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
  };

string depth_search(TreeNode * root,unordered_map <string,int> &hm,vector <TreeNode*> & ans)
{
    if(root == NULL)
        return "#";
    string serial = to_string(root->val) + "," +
        depth_search(root->left,hm,ans) + "," +
        depth_search(root->right,hm,ans);
    if(hm.count(serial) != 0)
    {
        hm[serial]++;
        if(hm[serial] == 2)
        {
            ans.push_back(root);
        }
    }
    else
    {
        hm[serial] = 1;
    }
    return serial;
}


vector<TreeNode*> findDuplicateSubtrees(TreeNode* root) 
{
    vector <TreeNode *> ans;        
    unordered_map <string,int> hm;
    depth_search(root,hm,ans);
    return ans;
}

int main()
{
}
