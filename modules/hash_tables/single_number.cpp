#include <vector>
#include <unordered_set>
using namespace std;

/*
 * In the given array, every element appears
 * twice except one, return that element.
 * For explanation of xor implementation 
 * refer this link ->
 * https://leetcode.com/problems/single-number/solution/
 */

int singleNumber(vector<int>& nums) 
{
    unordered_set <int> hs;

    for(auto i = nums.begin();i != nums.end();i++)
    {
        if(hs.count(*i) == 0)
        {
            hs.insert(*i);
        }
        else if(hs.count(*i) == 1)
        {
            hs.erase(*i);
        }
    }

    for(auto i = nums.begin();i != nums.end();i++)
    {
        if(hs.count(*i) == 1)
        {
            return *i;
        }
    }
    return 1;    
}

int singleNumber_xor(vector <int> nums)
{
    int a = 0;
    for(auto i = nums.begin();i != nums.end();i++)
    {
        a = a^(*i);
    }
    return a;
}

int main()
{
}
