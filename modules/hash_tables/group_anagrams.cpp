#include <bits/stdc++.h>
#include <unordered_map>
#include <vector>
using namespace std;

/*
 * Given an array of strings, group the anagrams 
 * together
 */

vector<vector<string>> groupAnagrams(vector<string>& strs) 
{
    unordered_map <string,vector<string>> hm;
    vector <vector <string>> ans;
    for(auto i = strs.begin(); i != strs.end();i++)
    {
       string tmp = *i;
       sort(tmp.begin(),tmp.end());
       hm[tmp].push_back(*i);
    }

    for(auto i = hm.begin();i != hm.end();i++)
    {
        ans.push_back(i->second);
    }

    return ans;
}

int main()
{
}
