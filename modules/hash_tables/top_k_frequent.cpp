#include <algorithm>
#include <unordered_map>
#include <vector>
using namespace std;

/*
 * Given an array of integers, return the k 
 * most frequent integers
 */

vector<int> topKFrequent(vector<int>& nums, int k) 
{
    unordered_map <int,int> hm1;
    unordered_map <int,vector <int>> hm2;
    vector <int> elements;
    vector <int> ans;

    for(int i : nums)
    {
        hm1[i]++;
    }

    for(auto i : hm1)
    {
        if(hm2.count(i.second) == 0)
        {
            elements.push_back(i.second);
        }
        hm2[i.second].push_back(i.first);
    }

    sort(elements.begin(),elements.end());

    for(int i = elements.size()-1;i >= 0;i--) 
    {
        for(int j : hm2[elements[i]])
        {
            if(k == 0)
            {
                return ans;
            }
            ans.push_back(j);
            k--;
        }
    }

    return ans;
}

int main()
{
}
