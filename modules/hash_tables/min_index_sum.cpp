#include <unordered_map>
#include <vector>
using namespace std;

/*
 * Given two lists of strings, return the common string(s)
 * with the least index sum.
 */

vector<string> findRestaurant(vector<string>& list1, vector<string>& list2) 
{
    vector <string> ans;
    int min = 2001;
    unordered_map <string,int> hm;
    for(int i = 0;i < list1.size(); i++)
    {
        hm[list1[i]] = i;
    }

    for(int i = 0;i < list2.size();i++)
    {
        if(hm.count(list2[i]) != 0)
        {
            int indexsum = hm[list2[i]] + i;
            if(indexsum < min)
            {
                ans.clear();
                ans.push_back(list2[i]);
                min = indexsum;
            }
            else if(indexsum == min)
            {
                ans.push_back(list2[i]);
            }
        }
    }

    return ans;
}

int main()
{
}
