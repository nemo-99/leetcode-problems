#include <unordered_map> 
#include <vector>
using namespace std;

/*
 * Given an array and a target integer,
 * return indices of two integers in the array
 * that add upto the target integer
 */

vector<int> twoSum(vector<int>& nums, int target) 
{
    vector <int> ans;
    unordered_map <int,int> hm;
    for(int i = 0;i < nums.size();i++)
    {
        if(hm.count(target - nums[i]) > 0)
        {
            ans.push_back(hm[target - nums[i]]);
            ans.push_back(i);
            return ans;
        }
        hm[nums[i]] = i;
            
    }
    return ans; 
}

int main()
{
}
