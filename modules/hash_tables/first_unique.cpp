#include <unordered_map>
using namespace std;

/*
 * Find first unique character in a
 * string and return its index. Else
 * return -1
 */

int firstUniqChar(string s) 
{
    unordered_map <char, int> hm;
    for(int i = 0;i < s.size();i++)
    {
        if(hm.count(s[i]) != 0)
        {
            hm[s[i]]++;
        }
        else
        {
            hm[s[i]] = 1;
        }
    }

    for(int i = 0;i < s.size();i++)
    {
        if(hm[s[i]] == 1)
        {
            return i;
        }
    }

    return -1;
}

int main()
{
}
