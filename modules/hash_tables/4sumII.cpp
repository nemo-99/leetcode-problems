#include <vector>
#include <unordered_map>
using namespace std;

/*
 * Given four lists A,B,C,D of equal length, return
 * the number of tuples such that A[i] + B[j] + C[k]
 * + D[l] = 0
 */

int fourSumCount(vector<int>& A, vector<int>& B, vector<int>& C, vector<int>& D) 
{
    unordered_map <int,int> hsAB,hsCD;
    int count = 0;
    for(int i = 0;i < A.size();i++)
    {
        for(int j = 0;j < B.size();j++)
        {
            hsAB[A[i] + B[j]]++;
            hsCD[C[i] + D[j]]++;
        }
    }

    for(auto i = hsAB.begin();i != hsAB.end();i++)
    {
        if(hsCD.count(-i->first) != 0)
        {
            count += hsAB[i->first] * hsCD[-i->first];
        }
    }

    return count;
}

int main()
{
}
