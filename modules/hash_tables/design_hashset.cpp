#include <vector>
using namespace std;

/*
 * Hashset to hold numbers in the range
 * of [1,1000000]
 */
class MyHashSet {
public:
    
    int * hashtable;

    MyHashSet() {
        hashtable = new int[1000001];
    }
    
    void add(int key) {
        hashtable[key]++;
    }
    
    void remove(int key) {
       hashtable[key] = 0; 
    }
    
    /** Returns true if this set contains the specified element */
    bool contains(int key) {
       return hashtable[key] == 1; 
    }

};

int main()
{
}
