#include <functional>
#include <iostream>
#include <vector>
#include <unordered_set>

/*
 * Check if a given array contains duplicates 
 * using a hash set
 */


bool containsDuplicate(std::vector<int>& nums) 
{
    if(nums.empty())
    {
        return false;
    }
    std::unordered_set <int> hs;
    for(auto i = nums.begin();i != nums.end();i++)
    {
       if(hs.count(*i) > 0)
       {
           return true;
       }

       hs.insert(*i); 
    }

    return false;
}

int main()
{
    std::vector <int> test = {2,3,1};
    std::cout << containsDuplicate(test) << std::endl;
}
