#include <unordered_map>
#include <vector>
using namespace std;

/*
 * Find intersection of two arrays
 * Each element in the result should appear
 * exactly as many times as in the input arrays
 */

vector<int> intersect(vector<int>& nums1, vector<int>& nums2) 
{
    unordered_map <int,int> hm;        
    vector <int> ans;
    for(auto i = nums1.begin();i != nums1.end();i++)
    {
        if(hm.count(*i) == 0)
        {
            hm[*i] = 1;
        }
        else
        {
            hm[*i]++;
        }
    }

    for(auto i = nums2.begin();i != nums2.end();i++)
    {
        if(hm.count(*i) != 0 && hm[*i] != 0)
        {
            ans.push_back(*i);
            hm[*i]--;
        }
    }

    return ans;

}

int main()
{
}

