#include <unordered_set>
#include <string>
#include <vector>
using namespace std;

/*
 * Check if a given sudoku matrix is valid 
 * or not
 */

bool isValidSudoku(vector<vector<char>>& board) 
{
    unordered_set <string> box;
    unordered_set <string> row;
    unordered_set <string> col;
    for(int i = 0;i < board.size();i++)
    {
        for(int j = 0;j < board[i].size();j++)
        {
            char val = board[i][j];
            if(val == '.')
            {
                continue;
            }
            string row_no = to_string(i);
            string col_no = to_string(j);
            string box_no;
            if(i <=2)
            {
                if(j<=2)
                    box_no = "1";
                else if(j<=5)
                    box_no = "2";
                else
                    box_no = "3";
            }
            else if(i <= 5)
            {
                if(j<=2)
                    box_no = "4";
                else if(j<=5)
                    box_no = "5";
                else
                    box_no = "6";
            }
            else
            {
                if(j<=2)
                    box_no = "7";
                else if(j<=5)
                    box_no = "8";
                else
                    box_no = "9";
            }

            if(box.count(box_no+val) != 0 || row.count(row_no+val) != 0 || 
                    col.count(col_no+val))
            {
                return false;
            }
            else
            {
                box.insert(box_no+val);
                row.insert(row_no+val);
                col.insert(col_no+val);
            }
        }
    }

    return true;
}

int main()
{
}
