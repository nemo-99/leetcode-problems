#include <unordered_set>
using namespace std;

/*
 * Given two strings J and S, return how 
 * many chars in S are a part of J
 */

int numJewelsInStones(string J, string S) 
{
    unordered_set <char> hs;
    int ans = 0;
    for(int i = 0;i < J.size();i++)
    {
        hs.insert(J[i]);
    }

    for(int i = 0; i < S.size();i++)
    {
        if(hs.count(S[i]))
        {
            ans++;
        }
    }

    return ans;

}

int main()
{
}
