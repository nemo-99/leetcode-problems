/*
    Given a linked list sorted in ascending order, remove
    nodes that have duplicate values
    Input: head = [1,2,3,3,4,4,5]
    Output: [1,2,5]
*/

package SixtyImportantQuestions;

public class RemoveDupliFromSortedListII {
    public class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }

    public ListNode deleteDuplicates(ListNode head) {
        ListNode newTmpNode = null;
        ListNode newHead = null;

        while (head != null) {
            if (head.next == null) {
                if (newTmpNode == null) {
                    newTmpNode = new ListNode(head.val, null);
                    newHead = newTmpNode;
                } else {
                    newTmpNode.next = new ListNode(head.val, null);
                }
                head = head.next;
            }

            else if (head.next != null && head.next.val == head.val) {
                while (head.next != null && head.next.val == head.val) {
                    head = head.next;
                }
                head = head.next;
            } else {
                if (newTmpNode == null) {
                    newTmpNode = new ListNode(head.val);
                    newHead = newTmpNode;
                    head = head.next;
                } else {
                    newTmpNode.next = new ListNode(head.val);
                    head = head.next;
                    newTmpNode = newTmpNode.next;
                }
            }
        }
        return newHead;
    }
}
