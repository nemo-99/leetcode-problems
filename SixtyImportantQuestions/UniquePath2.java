package SixtyImportantQuestions;

/*
    Starting from the top left position of a grid,
    find the number of unique paths to the bottom right
    position provided that there are obstacles at certain positions
    Input: obstacleGrid = [[0,0,0],[0,1,0],[0,0,0]]
    Output: 2
 */

public class UniquePath2 {
    public int uniquePathsWithObstacles(int[][] obstacleGrid) {
        int m = obstacleGrid.length;
        int n = obstacleGrid[0].length;
        int answers[][] = new int[m][n];

        for (int i = m - 1; i >= 0; i--) {
            for (int j = n - 1; j >= 0; j--) {
                if (obstacleGrid[i][j] == 1) {
                    answers[i][j] = 0;
                } else if (i == m - 1 && j == n - 1) {
                    answers[i][j] = 0;
                } else if (i == m - 1) {
                    answers[i][j] = answers[i][j + 1];
                } else if (j == n - 1) {
                    answers[i][j] = answers[i + 1][j];
                } else {
                    answers[i][j] = answers[i + 1][j] + answers[i][j + 1];
                }
            }
        }

        return answers[0][0];
    }

    public static void main(String[] args) {
        int obstacleGrid[][] = { { 0, 0 }, { 1, 1 }, { 0, 0 } };
        UniquePath2 uniquePath2 = new UniquePath2();
        System.out.println(uniquePath2.uniquePathsWithObstacles(obstacleGrid));
    }
}
