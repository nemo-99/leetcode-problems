package SixtyImportantQuestions;

/*
    Just like Best time to Buy and Sell Stock 1 but allows 
    for multiple transactions
    Input: prices = [7,1,5,3,6,4]
    Output: 7
*/

public class BestBuySell2 {

    // My Solution
    public int maxProfit(int[] prices) {
        int len = prices.length;
        int[] answers = new int[len + 1];
        int[] max = new int[len + 1];

        for (int i = len - 2; i >= 0; i--) {
            for (int j = i + 1; j <= len - 1; j++) {
                answers[i] = Math.max(answers[i], prices[j] - prices[i] + max[j + 1]);
            }
            max[i] = Math.max(answers[i], max[i + 1]);
        }
        return max[0];
    }

    // Better solution obtained from leetcode
    public int maxProfitLeetCode(int[] prices) {
        int max = 0;
        for (int i = 1; i < prices.length; i++) {
            if (prices[i] > prices[i - 1]) {
                max += prices[i] - prices[i - 1];
            }
        }
        return max;
    }

    public static void main(String[] args) {
        int[] prices = { 7, 1, 5, 3, 6, 4 };
        BestBuySell2 bestBuySell2 = new BestBuySell2();
        int answer = bestBuySell2.maxProfitLeetCode(prices);
        System.out.println(answer);
    }

}
