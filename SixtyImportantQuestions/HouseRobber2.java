package SixtyImportantQuestions;

/*
    Given an array of integers representing the loot in each house,
    find the maximum loot a robber can rob given that looting adjacent
    houses are not allowed and the houses are in a circular array
    Input: nums = [1,2,3,1]
    Output: 4
*/

public class HouseRobber2 {
    public int rob(int[] nums) {
        if (nums.length == 1) {
            return nums[0];
        }

        int len = nums.length;
        int max = nums[len - 1] > nums[len - 2] ? nums[len - 1] : nums[len - 2];
        int[] answersSkipFirst = new int[len];
        int[] answersSkipLast = new int[len];

        answersSkipFirst[len - 1] = nums[len - 1];
        answersSkipFirst[len - 2] = nums[len - 2];
        answersSkipLast[len - 1] = nums[len - 1];
        answersSkipLast[len - 2] = nums[len - 2];

        // skip first element
        for (int i = len - 3; i >= 1; i--) {
            for (int j = i + 2; j < len; j++) {
                answersSkipFirst[i] = Math.max(answersSkipFirst[i], nums[i] + answersSkipFirst[j]);
            }
            if (answersSkipFirst[i] > max) {
                max = answersSkipFirst[i];
            }
        }

        // skip last element
        for (int i = len - 3; i >= 0; i--) {
            for (int j = i + 2; j < len - 1; j++) {
                answersSkipLast[i] = Math.max(answersSkipLast[i], nums[i] + answersSkipLast[j]);
            }
            if (answersSkipLast[i] == 0) {
                answersSkipLast[i] = nums[i];
            }
            if (max < answersSkipLast[i]) {
                max = answersSkipLast[i];
            }
        }

        return max;
    }

    public static void main(String[] args) {
        int[] nums = { 8, 4, 8, 5, 9, 6, 5, 4, 4, 10 };
        HouseRobber2 houseRobber2 = new HouseRobber2();
        System.out.println(houseRobber2.rob(nums));
    }
}
