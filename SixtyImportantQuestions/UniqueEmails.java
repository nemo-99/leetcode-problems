/*
    Unique Email Addresses

    Given, a list of emails, return the number of unique names
    Each email consists of hostname and domain name separated by @ symbol

    If a . is present in hostname, it'll be ignored
    If a + is present in hostname, the rest of the hostname will be ignored 

    Input: emails = ["test.email+alex@leetcode.com","test.e.mail+bob.cathy@leetcode.com","testemail+david@lee.tcode.com"]
    Output: 2
*/

package SixtyImportantQuestions;

import java.util.HashSet;

public class UniqueEmails {
    public int numUniqueEmails(String[] emails) {
        HashSet<String> hashSet = new HashSet<>();
        for (String email : emails) {
            String domainName = email.split("@")[1];
            String processedEmail = "";
            for (int i = 0; i < email.length(); i++) {
                if (email.charAt(i) == '@' || email.charAt(i) == '+') {
                    break;
                } else if (email.charAt(i) == '.') {
                    continue;
                }
                processedEmail += email.charAt(i);
            }
            processedEmail += '@';
            processedEmail += domainName;
            if (!hashSet.contains(processedEmail)) {
                hashSet.add(processedEmail);
            }

        }
        return hashSet.size();
    }

    public static void main(String[] args) {
        UniqueEmails eUniqueEmails = new UniqueEmails();
        String emails[] = { "test.email+alex@leetcode.com", "test.e.mail+bob.cathy@leetcode.com",
                "testemail+david@lee.tcode.com" };
        System.out.println(eUniqueEmails.numUniqueEmails(emails));
    }
}
