package SixtyImportantQuestions;

/*
    Starting from the top left position of a grid,
    find the number of unique paths to the bottom right
    position
    Input: m = 3, n = 7
    Output: 28
 */

public class UniquePath {
    public int uniquePaths(int m, int n) {
        int answers[][] = new int[m][n];
        for (int i = m - 1; i >= 0; i--) {
            for (int j = n - 1; j >= 0; j--) {
                if (i == m - 1 && j == n - 1) {
                    answers[i][j] = 1;
                } else if (i == m - 1) {
                    answers[i][j] = 1;
                } else if (j == n - 1) {
                    answers[i][j] = 1;
                } else {
                    answers[i][j] = answers[i + 1][j] + answers[i][j + 1];
                }
            }
        }
        return answers[0][0];
    }

    public static void main(String[] args) {
        UniquePath uniquePath = new UniquePath();
        System.out.println(uniquePath.uniquePaths(3, 7));
    }
}
