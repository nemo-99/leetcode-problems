package SixtyImportantQuestions;

/*
 Given an neighborhood of houses to steal money from, return the maximum
 money you can steal provided you cannot steal from adjacent houses.
 You're given an array of integers signifying the money stashed in each house
 Input: nums = [2,7,9,3,1]
 Output: 12
*/

public class HouseRobber {
    int results[];

    public static void main(String[] args) {
        int nums[] = { 2, 1, 1, 2 };
        HouseRobber houseRobber = new HouseRobber();
        // For using memoization method change houseRobber.rob(nums) to
        // houseRobber.rob_memo(nums)
        System.out.println(houseRobber.rob(nums));
    }

    public int rob(int[] nums) {
        int len = nums.length;
        if (len == 1) {
            return nums[0];
        }

        int results[] = new int[len];

        results[len - 1] = nums[len - 1];
        results[len - 2] = nums[len - 2];

        for (int i = len - 3; i >= 0; i--) {
            int max = -1;
            for (int j = i + 2; j < len; j++) {
                max = Math.max(max, nums[i] + results[j]);
            }
            results[i] = max;
        }

        return results[0] > results[1] ? results[0] : results[1];
    }

    // Memoization
    public int rob_memo(int[] nums) {
        results = new int[nums.length];
        for (int i = 0; i < results.length; i++) {
            results[i] = -1;
        }

        int max = -1;

        for (int i = 0; i < nums.length; i++) {
            max = Math.max(max, rob_aux(nums, i));
        }

        return max;

    }

    public int rob_aux(int[] nums, int i) {

        int max = nums[i];
        if (results[i] >= 0) {
            return results[i];
        }

        for (int j = i + 2; j < nums.length; j++) {
            max = Math.max(max, nums[i] + rob_aux(nums, j));
        }

        results[i] = max;
        return max;
    }

}
