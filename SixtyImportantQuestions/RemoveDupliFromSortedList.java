/*
    Given a Sorted List, remove all duplicates
    Input : 1->1->2->3->3
    Output : 1->2->3
*/

package SixtyImportantQuestions;

class RemoDupliFromSortedList {
    public class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }

    public ListNode deleteDuplicates(ListNode head) {
        ListNode tmpNode = head;

        while (tmpNode != null && tmpNode.next != null) {
            while (tmpNode.next != null && tmpNode.next.val == tmpNode.val) {
                tmpNode.next = tmpNode.next.next;
            }
            tmpNode = tmpNode.next;
        }
        return head;
    }
}