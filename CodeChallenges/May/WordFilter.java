import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/*
Implement the WordFilter class with the following functionalities
parameterized constructor  public WordFilter(String[] words) to take total words in dictionary as input
function public int f(String prefix, String suffix) that takes two arguments prefix and suffix
and return -1 if no words start with prefix and end with suffix
else it will return the index of the last word with the matching prefix and suffix
Input
["WordFilter", "f"]
[[["apple"]], ["a", "e"]]
Output
[null, 0]
*/

class WordFilter {

    Map<String, Integer> wordIndex;

    public WordFilter(String[] words) {
        wordIndex = new LinkedHashMap<>();
        for (int i = 0; i < words.length; i++) {
            wordIndex.put(words[i], i);
        }
    }

    public int f(String prefix, String suffix) {
        int idx = -1;
        Set<String> words = wordIndex.keySet();
        for (String word : words) {
            if (word.startsWith(prefix) && word.endsWith(suffix)) {
                idx = wordIndex.get(word);
            }
        }

        return idx;
    }

    public static void main(String[] args) {
        String[] words = { "cabaabaaaa", "ccbcababac", "bacaabccba", "bcbbcbacaa", "abcaccbcaa", "accabaccaa",
                "cabcbbbcca", "ababccabcb", "caccbbcbab", "bccbacbcba" };
        WordFilter wordFilter = new WordFilter(words);
        System.out.println(wordFilter.f("a", "aa"));
    }
}
